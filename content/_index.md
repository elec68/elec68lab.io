

Propulsé par [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) ; Thème utilisé : [docdock](https://docdock.netlify.com/)

Le projet gitlab correspondant :  
    <https://gitlab.com/elec68/elec68.gitlab.io/>

*_Site web me servant de bloc-notes et de partage._*

Front-matter des articles :
```
---
title: 1er post!
date: 2018-01-05
tags: debian
categories:
draft: false
chapter:
weight:
pre: "<b>1. </b>"
---
This is my first post
```

Ressources :  
<http://pingouindesalpes.com/hugo/project/hugo/>  
<https://jamstatic.fr/2017/08/28/un-site-ecommerce-statique-avec-hugo/#tutoriel>


Mon compte Mastodon :
<a rel="me" href="https://mamot.fr/@rp68">Mastodon</a>
