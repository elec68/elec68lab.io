---
title:  Les raisons pour privilégier un site Statique
date: 2018-01-05
draft: true
---

## Objectif
Création d'un Outils de communication collaboratif ayant les caractéristiques suivantes :
  - Simple,
	 >_Nécessite très peut de temps dans sa mise en oeuvre._
  - Ultra peut chère,
     >_Idéalement Gratuit_
  - Collaboratif,
    >_Aisément utilisable sans compétence particulière par la communauté concernée_
  - Réactif,
     >_Permettre une mise à jour sans prise de tête, avec les outils déjà utilisés.
     Extraction des articles à publier depuis la base de donnée documentaire `Couchdb`. ÉEdition des articles a l'aide du composeur Markdown `Stackedit` qui assure la persistance des sources nativement dans la la base `CouchDB`_

### Remarque :
Avant tout la simplicité que j’évoque concerne la simplicité d'usage. Si la mise en place du dispositif est élégante et simple cela est un plus, mais ne constitue pas l'objectif. Souvent ce qui semble simple d'usage, est avant tout de la complexité masquée.

## Projets candidats

(l’étude peut être complétéer)  

1### 1\.__1) La solution `CMS` :__

On peut envisager d'utiliser l'un des nombreux `CMS` [Web Content Management System](https://en.wikipedia.org/wiki/Web_content_management_system) Il en existe des centaines voire des milliers (plusieurs centaines même en Libre). [WordPress ](https://fr.wikipedia.org/wiki/WordPress) estlafabrikdigitale l'un des plus populaire, globalement cette démarche basé sur un CMS peut satisfaire aux critères définis précédemment.
La cible type d'un CMS, c'est la construction d'un site institutionnel, un site multi-services de présentation de l'activité d'une entreprise par exemple.
On pourrait se dire qui peut le plus peut le moins, ce n'est pas forcement la bonne approche pourquoi ?
Pour en avoir utilisé depuis plus de 15 ans, au début (vers 2000)  c’était `Mambo` qui est devenue `Joomla`, puis je me suis essayer à `Zend` et à `Plone` l'un de ses dérivé, aussi `Wordpress` avec son avatar de langage `PHP` (prisé une époque par la communauté mais une vrai gageure technologiquement), j'ai migré vers `Drupal` avant de revenir sur des langages plus mur comme `Python` avec `Django`. Bref systématiquement des usines à Gaz avec un gros `SGBD` en `SQL` standard, délicat à faire évoluer, a maintenir d'en 'assurer la  sécurité comme les synchro's et/ou réplications.

Pour des actions qui se veulent dynamique et réactives, bizarrement c'est pas la bonne voie.


2### 2\.__2) L’alternative : Un Blog statique auto-généréer__

Quelle est la pertinence de créer un Site/Blog communautaire a l'aide d'un CMS massif et encombrant avec une base de données SQL Mastoc, alors que l'on peut avoir un site statique rapide, sécurisé et beau.
Quels sont les éléments a considérer, pour savoir  si un site statique peu convenir ?

### 3\. Les 9 raisons pour privilégier un site Statique
<!---
https://www.netlify.com/blog/2016/05/18/9-reasons-your-site-should-be-static/
--->

####  1) __Question sécurité :__
   __S__ Selon une estimation prudente, [70 % de toutes les installations `WordPress`](https://www.wpwhitesecurity.com/wordpress-security-news-updates/statistics-70-percent-wordpress-installations-vulnerable/) sont vulnérables aux exploits de sécurité connus. En 2015, des [millions de sites `Drupal`](https://blog.sucuri.net/2014/10/drupal-warns-every-drupal-7-website-was-compromised-unless-patched.html) étaient vulnérables à cause d'un bug dans le code, et [`Drupal` a dit aux utilisateurs](https://www.drupal.org/PSA-2014-003) que s'ils n'avaient pas mis à jour dans les 7 heures suivant l'annonce, ils devraient se considéerer commeupposer qu'ils éetaient piratés.


__*Avec un site statique*, aucune inquiétude__ quand à l'injection de code malveillant. Les sites statiques sont construits sur une machine de production par des générateurs de site statiques, qui prennent les sources documentaire à l'aide d'un code et recrachent des fichiers HTML plats avec CSS et JavaScript. Lorsqu'un utilisateur demande une page à partir du site, le serveur lui envoie simplement le fichier pour cette page, au lieu de construire cette page à partir de différents base de données à chaque fois. Aucun processus de compilation signifie que les attaques de piratage standard comme les scripts ou les exploits de sécurité sur les bases de données, simplement car ces éléments sont inexistants.

- __Question performance :
   Q__ ue fait un navigateur? Il affiche les fichiers HTML, CSS et JavaScript dans un format humain. Quel que soit le type de site, qu'il s'agisse d'un site dynamique existant ou d'un site statique moderne, ce qui est envoyé du serveur à votre navigateur c'est HTML, CSS et JavaScript. Alors, pourquoi prendre du temps, des ressources pour reconstruire chaque page a chaque visite alors qu'avec un site statique tous est prêt ?

   __C__ __Ce n'est pas par hasard qu'un site statique auto-généré est souvent 10 fois plus rapide en temps de réponse qu'un site construit avec un `CMS` traditionnel.

   __D__ __De plus, l'hébergement d'un site statique sur un [CDN](https://www.ovh.com/fr/cdn/avantages.xml) ([Content Delivery Network](https://www.ovh.com/fr/cdn/ "Le CDN OVH")), est plus facilement localisé au plus proche des utilisateurs. Quelqu'un qui va consulter le site dans un café-restaurant à Strasbourg, se sert d'un serveur européen, pas d'une ferme de serveurs dans la Silicon Valley.

#### 3) - __Question  - __Souplesse :
   __U__ n site de base construit avec `WordPress` ou `Drupal` est d'abord une solution unique qui est ensuite personnalisée en installant des plugins. Beaucoup de plugins*. Sérieusement, tant de plugins. Une recherche Google rapide pour "must have WordPress plugins" donne une page d'accueil où des dizaines de pages qui listent 20 plugins ou plus comme "must have".
   Avec un site statique, on obtiens juste de l’efficience, juste ce que l'on a besoin, rien de plus pas de gras.
   En choisissant l'outil de génération, il fera exactement ce que l'on a besoin.

   - Si  c'est pour faire un Blog ? Alors cela peut être [Jekyll](https://jekyllrb.com/) [^1] ou [Hugo](https://gohugo.io/).
     - On souhaite un site marketing performant? Faites comme les pros et utilisez [Middleman](https://middlemanapp.com/) ou [Roots](https://github.com/jescalan/roots).    
     - On souhaite mettre en valeur un [`portfolio`](https://fr.wikipedia.org/wiki/Portfolio) visuel? Peut-être que [Cactus](https://github.com/eudicots/Cactus) sera l'outil idéal, surtout pour les amoureux de python.    
     - Et si vous voulez quelque chose de si finement ajusté et rationalisé selon vos spécifications complètes, [Metalsmith](http://www.metalsmith.io/) est définitivement la solution.

	 Je vous donne ma pépite extrême pour faire votre choix, alors consulter sans exagération et avec excès [staticgen](https://www.staticgen.com/).

[^1]: j’ai abandonné [Wordpress](https://fr.wikipedia.org/wiki/WordPres://fr.wordpress.org/) pour [Jekyll](https://jekyllrb.com/) : [Pourquoi et comment créer un site statiques avec Jekyll](https://blog.rom1v.comommentaireatiques-avec-jekyll/)


####	 
   - __Question de Empreinte réduite :
    U__ ne installation `WordPress` (surtout en auto-hébergement) est un monstre de Frankenstein composé de logiciels et de matériel, et ressemble probablement à ceci :
      - Une machine exécutant votre distribution préférée de Linux
      - Un serveur web exécutant Nginx ou Apache
      - Une config PHP avec ses extensions associées et ses configurations de serveur web :
	      - MySQL
	      - WordPress (évidemment)
	      - Tous les plugins dont vous avez besoin _(à ce jour  sur le site officiel "Wordpress" on denombre **54.670 plugins**, facile pour trouver son bonheur [^2])_(voir #3)
	      - Votre thème et votre code modèle


	 Dans un mode hébergé, il faut espérer que votre hébergeur garde son __PHP__ et __MySQL__ à jour, afin que vous ne soyez pas exposé à ces failles de sécurité qui surgissent de temps en temps.
	 Ensuite, il y a l'entretien. Assurez-vous d'avoir le temps de gérer toutes ces dépendances. Et un peu plus de temps au cas où un plugin ou un thème mis à jour casserait quelque chose.

	 Un site statique, une fois généré, est capable d'être hébergé sur n'importe quel serveur web qui peut renvoyer des fichiers __HTML __(ce qui vous donne tout un tas d'options). Bien sûr, vous voudrez bien profiter des possibilités qui s'offrent à vous avec un site statique en trouvant un hôte qui permet des choses comme le déploiement continu, l'invalidation instantanée du cache, les déploiements automatisés et plus encore. Mais vous pouvez laisser cela à quelqu'un d'autre, et au lieu d'installer, de gérer et de mettre à jour votre `CMS`, vous pouvez vous concentrer sur le développement de votre site.

[^2]:Liste des [Plugins WordPress](https://wordpress.org/plugins).

#### 5)    Question de la fiabilité :
 __ L__ ié a cette notion d'empreinte pesante du `CMS`, la fiabilité du site y est aussitôt impacté.

    - __Question de la fiabilité :
  A__ vec l'empreinte du `CMS` est directement lié sa fiabilité. C'est la lois des probabilités : plus on à d’éléments différencies liés, plus le risque de perdre l'un d'eux est grand et plus le risque globale est élevé. Mise a part l’énergie et la compétence de requière la maintenance de la pile des composants d'un `CMS`, on prête également le flanc a bien plus de contraintes et contingences externes. Supposons qu'il y ait une attaque DDoS sur le serveur où votre site est hébergé. Désolé, dans ce cas on est hors ligne pendant quelques heures (ou quelques jours).  Dans le cas d'un site statique localise en [CDN](https://www.ovh.com/fr/cdn/avantages.xml) même si cette attaque touche le nœud où votre site statique est hébergé? Votre site sera servi à partir du nœud en cache le plus proche. Vos visiteurs ne remarquent même pas qu'il y a un problème.

    - __Question de Contrôle de version :
  __I__ l est tout à fait possible de concevoir un site static sur ur votre machine de production, et de le télécharger sur un hôte (que ce soit via un outil en ligne de commande ou une interface glisser-déposer), mais la majorité des développeurs finira par utiliser une sorte de système de contrôle de version comme Git.
  En tant que développeur, vous savez probablement déjà à quel point cela peut vous sauver la vie sur des projets, mais c'est tout aussi important quand il s'agit de développer des sites Web.
  Les mises a jour peuvent être rejoué en sens inverse (RollBack) par rapport aux validations précédentes, ce qui signifie qu' à moins de supprimer votre référentiel en ligne, vous pouvez toujours revenir à une version précédente de votre site avec quelques frappes de touche.

    - __Question de Expérience développeur :
__    L__ a beauté d'utiliser un générateur de site web statique réside dans l'expérience du développeur. Les outils de construction (Build) produiront le code HTML vers un répertoire particulier sur votre machine, et presque tous les outils incluent un serveur web local, ce qui vous permet de vérifier et de vérifier votre progression au fur et à mesure. Vous avez la sécurité de savoir que votre site sera exactement le même pour vos visiteurs que pour vous en tant que développeur.

    - __Question de Évolutivité :
__    C__ 'est le point Mega fort du site Statique, ha bon et les autres alors ..!
    Si le site prend de l'ampleur (au mieux il devient viral). Une évolution de trafic impose quasi systématiquement un changement du mode hébergement pour un site `CMS` traditionnel. Il faut immanquablement changer d'échelle, cela signifie que l'on va payez pour tout ce code complexe à exécuter sur le serveur à chaque demande de page.
    Et ça va te frapper là où ça fait mal : __le portefeuille.__

      Avec  sitela statique, l'évolutivité n'est pas un problème.
      Bien sûr que ça s'ajuste! L'augmentation des demandes se traduit par une augmentation du nombre de pages servies, mais pas de travail supplémentaire dans la construction de ces pages. Avec certains fournisseurs, l'échelle est intégrée, tandis qu'avec d'autres comme Amazon S3, tout ce que vous avez à payer est l'augmentation de la bande passante.

#### 9)      - __Pour finir la qQuestion de l'Hébergement & du Prix :
    S__ i toute la lourde tâche de construction d'un site statique est réalise sur la machine de production, alors qu'est-ce que vous payez exactement avec l'hébergement ?

   __En Gros__ : c'est uniquement le prix du stockage des fichiers.
    Et là __les fichiers HTML statiques ne prennent__ _ pratiquement_ __ pas d'espace__, et votre service est donc généralement facturé en conséquence.
    Avec un site statique nos moyens sont engagés là où c'est vraiment important. Sur les fonctionnalités qui rendent votre site plus rapide et votre vie plus facile, ou l'intégration sur des espaces en mode git avec des compilations automatisées. De sorte que votre site est toujours à jour avec vos derniers changements, avec des images instantanée (snapshot) et l'ensemble du processus d'évolution (le versioning), permettant tout  les retours en arrière (dans le cas où vos derniers changements aurait casser quelque chose).

### 4\. Synthèse :
Alors qu'est-ce qui vous empêche maintenant de passer au monde du développement web statique ?
Le processus de migration semble-t-il trop intimidant ?
De nombreux outils comme Jekyll ont la possibilité d'importer les anciens messages WordPress.

__Quelque soit vos préférences__ de développement, qu'il s'agisse de __Node, Ruby, Python__ ou quelque chose d'autre, __il existe un générateur de site statique support__, pour chaue saveur d'un langage particulier.

y a un outil statique pour chacun. Je vous ai déjà proposé de jeté un coup d'oeil sur [staticgen](https://www.staticgen.com/), vous s'y trouverez de quoi apprendre d'avantage. Je pense que c'est certainement la meilleure source d'information sur les générateurs de sites web statiques sur Internet.

Il n' y a jamais eu de meilleur moment pour profiter de la vitesse, de la sécurité et de la fiabilité des générateurs web statiques et de l'environnement de développement qui les entoure.

J’espère que je vous ai maintenant convaincue, il nous reste a choisir et à mettre en oeuvre notre propre outil/site blog communautaire.

Bien entendu il convient de démarrer de notre existant, il convient donc d'en faire le constat.

## Point Pratique : La construction d'un site blog en mode Static

### Le constat actuel :
J'utilise avec beaucoup de bonheur depuis 3/4 ans, l’éditeur `en ligne/hors ligne` au format `Markdown` `Stackedit` qui me permet de créer ce document et tous les autres. Lié avec la base documentaire `CouchDB`, l'ensemble me garanti la persistance, la réplication, la sécurité, l'ubiquité d'utiliser mes documents sur tous les supports et plateformes existantes. Cela constitue la source des documents qui seront soumis à diffusions.

## Création d'un Blog "POC [^3]" en appli "design" dans CouchDB

[^3]: Proof Of Concept - [démonstration de faisabilité](https://fr.wikipedia.org/wiki/Preuve_de_concept)

### Presentation du projetEssai directement sous hebergement dans CouchDB

####  Préliminaire : Rappel CouchDB
CouchDB, est une base de données open source optimisé pour sa facilité d'utilisation et sur le fait d'être "une base de données conçu pour internetmplètement webisé". C'est une base de données NoSQL qui utilise le format JSON pour stocker les données, JavaScript comme langage de requête en utilisant MapReduce, et HTTP opour une API. L'une de ses caractéristiques distinctives est la réplication multi-maîtres. CouchDB a été lancé pour la première fois en 2005 et est devenu plus tard un projet Apache en 2008.

Je vais reprendre rapidement les principe de CouchDB. Vous apprendrez comment installer et configurer CouchDB et comment exécuter des opérations courantes avec lui.
Vous ne serez pas surpris que je préfere l'installation via un conteneur docker.

Ensuite nous allons essayer de construire ensemble un exemple d'application de type Blog à partir de zéro, pour terminer avec des sujets plus avancés comme la mise à l'échelle, la réplication et l'équilibrage de charge.

 ### [Installation de CouchDb - Comment installer CouchDB]()

Dans la première partie, je présente comment installer CouchDB sur une distribution Ubuntu. Pour les plus hardcore d'entre vous, on pourra voir comment faire une installation à partir de sources.


### [CouchDB: Notions de base et opérations](#Notions-de base-et-opérations)

Dans cette partie, vous serez introduit aux principes de base de CouchDB et aux opérations qui l'accompagnent. Dans un premier temps, l'interface Web Futon est discutée, puis les différentes opérations CRUD sont expliquées en détail. Des exemples de commandes API se référant au serveur, aux bases de données, aux documents et à la réplication sont fournis. Enfin, vous verrez comment travailler avec les documents et comment gérer les différentes révisions de ceux-ci.


#### [CouchDB: Documents de conception]()

Ici, nous mettrons l'accent sur les documents de conception, un type particulier de document CouchDB qui contient le code d'application. Nous verrons comment utiliser l'API pertinente pour créer, visualiser, valider des documents, etc.

#### [Construire une application Blog avec CouchDB]()

Dans cette section, nous allons construire une application de blog en utilisant le Javascript basé sur Web Server Node. js avec CouchDB. Pour cela, nous utiliserons le moteur de template javascript côté client swig, Node. js pour le développement côté serveur, middleware avec express. js, CouchDb comme base de données et Node. js Cradle Module Extension (pour réaliser la communication avec CouchDB).


#### [Déploiement et optimisation de CouchDB]()

Dans cette partie, nous explorerons quelques concepts plus avancés, tels que la mise à l'échelle de notre serveur CouchDB, la réplication, l'équilibrage de charge et le clustering. Enfin, nous verrons comment effectuer des tests de charge en utilisant le framework Tsung.

------------------------------1.  __Introduction__
	Apache CouchDB est une base de données open source NoSQL qui utilise JSON pour stocker des données, JavaScript comme langage de requête, utilise également MapReduce et HTTP pour une API. Dans CouchDB, chaque base de données est une collection de documents. Chaque document gère ses propres données et son propre schéma. Une application peut accéder à plusieurs bases de données sur différents serveurs. Les métadonnées de document contiennent les informations de révision nécessaires pour rendre la fusion possible au cas où les bases de données seraient déconnectées.

	Dans CouchDB, chaque document a un identifiant unique et il n' y a pas de schéma de document requis. CouchDB peut gérer un volume élevé de lecteurs et d'écrivains simultanés sans conflit. Les données stockées sont structurées à l'aide de vues. Chaque vue est construite par une fonction JavaScript qui agit comme la Carte. La fonction prend un document et le transforme en une seule valeur qu'elle retourne. CouchDB peut indexer les vues et tenir ces index à jour au fur et à mesure que des documents sont ajoutés, supprimés ou mis à jour.

	CouchDB est conçu pour la réplication et le fonctionnement hors ligne. Ici, plusieurs répliques peuvent avoir leurs copies des mêmes données, les modifier et les synchroniser ultérieurement. Toutes les opérations ont un URI unique qui est exposé via HTTP. Les API REST utilisent les méthodes HTTP POST, GET, PUT et DELETE pour les quatre opérations CRUD de base (Create, Read, Update, Delete) sur toutes les ressources.

	CouchDB est capable de s'auto-répliquer à d'autre instances. Ces instances pourront être déconnectés, la synchronisation des données sera gérer lorsque les instances seront a nouveaux connectés

###  CouchDB Installation sur Ubuntu

Selon la version Ubuntu, la disponibilité de CouchDB varie. Les versions plus récentes d'Ubuntu inclus CouchDB directement dans leurs référentiels. Nous pouvons installer CouchDB avec Ubuntu Software Center, ou depuis la ligne de commande avec les utilitaires apt-get ou aptitude. Cependant, pour obtenir la version la plus récente de CouchDB, il se peut que nous devions installer à partir des sources, ou d'autres dépôts de paquets qui ont des paquets CouchDB pré-construits plus récents.

#### Installation à l'aide d'un paquet existant

Ouvrez un terminal et tapez :

```sh
sudo  apt-get install  couchdb -y
```

**Dépannage:**  Si l'installation aptitude/apt-get donne un message d'erreur alors CouchDB pourrait ne pas avoir accès à son fichier pid dans Ubuntu Machine.

Pour résoudre ce problème, saisissez un terminal:
```sh
sudo  chown  -R couchdb /var/run/couchdb
```

Puis relancez le script d'installation :

```sh
sudo dpkg --configure couchdb
```

#### Installation à partir des sources  
Pour les version Ubuntu Precise, Quantal, Raring, et Saucy

Téléchargez les sources de CouchDB à partir d'un miroir apache ([http://www.apache.org/dyn/closer.cgi?path=couchdb/source/1.4.0/apache-couchdb-1.4.0.tar.gz](http://www.apache.org/dyn/closer.cgi?path=couchdb/source/1.4.0/apache-couchdb-1.4.0.tar.gz)).

assurez-vous d'avoir un utilisateur de couchdb pour le démon et le groupe divchb aussi
obtenir les dépendances des outils de développement
sudo apt-get install -y g+++
sudo apt-get install -y erlang-dev erlang-manpages erlang base-hipe erlang-eunit erlang-nox erlang-xmerl erlang-inets

1.  assurez-vous d'avoir un utilisateur et un group `couchdb`
2.  obtenir les dépendances des outils de développement
```sh
sudo apt-get install -y g++
sudo apt-get install -y erlang-dev erlang-manpages erlang-base-hipe erlang-eunit erlang-nox erlang-xmerl erlang-inets
```


### [CouchDB: Documents de conception]()
#### 1\. Introduction


Apache CouchDB est une base de données open source NoSQL qui utilise JSON pour stocker des données, JavaScript comme langage de requête et HTTP pour une API. Dans CouchDB, chaque base de données est en fait une collection de documents. Chaque document gère ses propres données et un schéma autonome. Une application peut accéder à plusieurs bases de données sur des serveurs différents et les métadonnées de document contiennent des informations de révision afin de rendre la fusion possible au cas où les bases de données seraient déconnectées.

Dans CouchDB, toutes les opérations ont un URI unique qui est exposé via HTTP. Les API REST utilisent les méthodes HTTP typiques POST, GET, PUT et DELETE pour les quatre opérations CRUD de base (Create, Read, Update, Delete) sur toutes les ressources. Enfin, Futon est la console d'administration Web de CouchDB. Voyons voir plus de détails pour ces composants.

#### 2\. L'interface d'administration "Futon"

Futon sera accessible par un navigateur via l'adresse suivante : [http://localhost:5984/_utils/](http://localhost:5984/_utils/)


La page de vue d'ensemble principale fournit une liste des bases de données et fournit l'interface pour interroger la base de données et créer et mettre à jour les documents.

##### 2.1 Les principales sections de Futon sont :Les principales sections de Futon sont:

Configuration:
Réplicateur: Une interface avec le système de réplication, nous permettant d'initier la réplication entre les bases de données locales et distantes.
Status: Affiche une liste des tâches d'arrière-plan en cours d'exécution sur le serveur. Les tâches de fond incluent la création d'index de vue, le compactage et la réplication. La page Status est une interface pour l'appel de l'API Tâches Actives.
Vérifier l'installation: L'installation de vérification nous permet de vérifier si tous les composants de l'installation de CouchDB sont correctement installés.
Test Suite: La section Test Suite nous permet d'exécuter la suite de test intégrée. Ceci exécute un certain nombre de routines de test entièrement dans le navigateur pour tester l'API et les fonctionnalités de l'installation CouchDB.


1.  **Configuration :**  
Une interface dans la configuration de l'installation du  e Wer CouchDB. L'interface permet d'éditer les différents paramètres configurables.
3.  **Réplicateur (Replicator) :**
Une interface avec le système de réplication, nous permettant d'initier la réplication entre les bases de données locales et distantes.
4.  **Status :**  
Affiche une liste des tâches d'arrière-plan en cours d'exécution sur le serveur. Les tâches de fond incluent la création d'index de vue, le compactage et la réplication. La page Status est une interface pour l'appel de l'API Tâches Actives.
5.  **Vérifier l'installation :**  
L'installation de vérification nous permet de vérifier si tous les composants de l'installation de CouchDB sont correctement installés.
6.  **Test Suite :**  
La section Test Suite, nous permet d'exécuter un ensemblla suite de test intégrée. Ceci exécute un certain nombre de routines de test entièrement dans le navigateur pour tester l'API et les fonctionnalités de l'installation CouchDB.

##### 2.21 Gestion des bases de données et des documents

Nous pouvons gérer les bases de données et les documents au sein de Futon en utilisant la section Aperçu de l'interface Futon.

Pour créer une nouvelle base de données, cliquez sur le bouton Créer une Ellipse de base de données. Le nom de la base de données nous sera demandé, comme le montre la figure ci-dessous.

![Create CouchDB Database](https://dc621.4shared.com/img/PVHKDhdaei/s23/1620f8ac358/create_db)

__Créer une base de données CouchDB__ -

Tapez le nom de la base de données (c'est-à-dire `msys_blog` ) dans la zone de texte que nous voulons créer.


 - Une fois que nous avons créé la base de données (ou sélectionné une base existante), nous recevrons la liste des documents en cours. Si nous créons un nouveau document ou si nous sélectionnons un document existant, l'affichage du document d'édition nous sera présenté.

 - Pour traiter les documents dans Futon, vous devez sélectionner le document, puis modifier (et paramétrer) les zones du document individuellement avant de le sauvegarder à nouveau dans la base de données.

Par exemple, la figure ci-dessous montre l'éditeur pour un seul document, un document nouvellement créé avec un seul ID, le champ Document _id (cliquez sur l'image pour l'afficher en taille réelle).

![New document](https://dc590.4shared.com/img/MWgc4A62ca/s23/1620f92ed30/1_document)

__Nouveau document__
Pour ajouter un champ au document:

1.  Cliquez sur Ajouter un champ ![add file](https://dc697.4shared.com/img/pT2QIIuXca/s23/1620f989e38/add-file).
2.  Dans la zone Nom du champ, saisissez le nom du champ. Par exemple, "nom_blogueur".
3.  Cliquez sur la coche verte à côté du nom du champ pour confirmer le changement de nom du champ.

![Changing a field name](https://dc697.4shared.com/img/StDJVLIKca/s23/1620f9c0168/nom_blogeur)]

4.  Double-cliquez sur la cellule de valeur correspondante.
5.  Saisissez un nom de société, par exemple "La_Fabrik_Digital".

![La_Fabrik_Digital](https://dc697.4shared.com/img/w6-pgSDkca/s23/1620f9ec470/La_Fabrik_Digital)


6.  Cliquez sur la coche verte à côté de la valeur du champ pour confirmer la valeur du champ.
7.  Nous devons enregistrer explicitement le document en cliquant sur le bouton Enregistrer le document en haut de la page  "![save-document](https://dc697.4shared.com/img/OOoKMQsYei/s23/1620fa63a98/save-document)". Ceci sauvegardera le document, puis affichera le nouveau document avec les informations de révision sauvegardées (champ _rev).

![Document changes](https://dc697.4shared.com/img/jdgFIArXei/s23/1620fa8a3c8/Document-changes)

##### 2.4 Configuration de la réplication

Lorsque vous cliquez sur l'option Réplication dans le menu Outils, l'écran Réplication s'affiche. ![replication](https://dc697.4shared.com/img/xAlUed15ei/s23/1620fb3b3d0/Replicator)
Cela nous permet de commencer la réplication entre deux bases de données en remplissant ou en sélectionnant les options appropriées dans le formulaire prévu à cet effet.

Par exemple, disons que nous avons une autre base de données 'test'. La liste des bases de données est donc la suivante:

![Databases Overview](https://dc600.4shared.com/img/t9jDBVG_ca/s23/1620fb847b0/repli_tst)
Aperçu des bases de données

Maintenant nous allons répliquer la base de données  `msys_blog` vers `repli_tst`.

Pour ce faire, cliquez sur `Réplicator` dans le panneau de droite.

Pour lancer le processus de réplication, sélectionnez la base de données locale ou saisissez un nom de base de données distante dans les zones correspondantes du formulaire. La réplication s'effectue de la base de données à gauche vers la base de données à droite.

Si nous spécifions un nom de base de données distante, nous devons spécifier l'URL complète de la base de données distante (incluant l'hôte, le numéro de port et le nom de la base de données). Si l'instance distante nécessite une authentification, nous pouvons spécifier le nom d'utilisateur et le mot de passe comme partie de l'URL, par exemple

http://username:pass@remotehost:5984/msys_blog.

![CouchDB Replication](https://dc600.4shared.com/img/XhI2R_Mdca/s23/1620fbfbdd8/base-replication)]
Réplication de bases en local

Pour activer la réplication continue, cochez la case Continu (continuous). Cliquez sur le bouton ![Réplicate](https://dc600.4shared.com/img/OpHkE8Bqei/s23/1620fc305b0/replicate).

Le processus de réplication devrait commencer et se poursuivra en arrière-plan. Si le processus de réplication prend beaucoup de temps, nous pouvons contrôler l'état de la réplication en utilisant l'option Statut dans le menu Outils.

Une fois la réplication terminée, la page affichera les informations en utilisant l'API CouchDB.

Le résultat sera affiché comme dans l'image suivante :



![Replication Result](https://dc600.4shared.com/img/YLKDOl-cei/s23/16210768ab8/Status-replication)

Statut de la réplication

Si nous ouvrons maintenant la base de données `repli_tst', nous trouverons une réplique exacte de la base de données `msys_blog` :

![Successful Replication](https://dc600.4shared.com/img/506lYOzvei/s23/162107b0728/Result_repli)

La replication est reussi

#### 3\. Operations CRUD

>Rappel sur le concept [CRUD](https://fr.wikipedia.org/wiki/CRUD) :
>


Je vous propose maintenant étudier rapidement l'interface de programmation d'applications (API) de CouchDB en utilisant l'utilitaire de ligne de commande "curl". Il nous donne le contrôle sur les requêtes HTTP brutes et nous pouvons voir exactement ce qui se passe sur la base de données.

Assurez-vous que CouchDB est toujours en cours d'exécution et puis à partir de la ligne de commande exécuter ce qui suit:

```sh
curl  http://127.0.0.1:5984/
```
Ceci envoie une requête GET à l'instance CouchDB nouvellement installée. La réponse devrait ressembler à quelque chose comme ça:

```
{"couchdb":"Welcome","version":"1.6.1"}
```

Ensuite, nous pouvons obtenir une liste des bases de données existantes:

```sh
curl -X GET http://127.0.0.1:5984/_all_dbs
```

Notez que nous avons ajouté la chaîne `_all_dbs` à la requête initiale... La réponse devrait ressembler:

```sh
["_replicator","_users","msys_blog","repli_tst"]
```

Il montre nos 2 bases de données nommées msys_blog et repli_tst qui ont été créés plus tôt via l'interface utilisateur Futon.

Créez une autre base de données, en utilisant l'API cette fois:

```sh
curl -X PUT http://127.0.0.1:5984/lfd_blog
```

En exécutant ceci, le CouchDB répondra avec:

```sh
{"ok":true}
```

L'extraction de la liste des bases de données affiche à nouveau quelques résultats utiles:
```sh
curl -X GET http://127.0.0.1:5984/_all_dbs
```

La console renvoie :
```sh
["_replicator","_users","lfd_blog","msys_blog","repli_tst"]
```

Créez une autre base de données avec le même nom:

```sh
curl -X PUT http://127.0.0.1:5984/lfd_blog
```

CouchDB répondra par :

```sh
	{"error":"file_exists","reason":"The database could not be created, the file already exists."}
```

Nous avons déjà une base de données avec ce nom, alors CouchDB, répondra par une erreur. Essayons à nouveau avec un nom de base de données différente :

```sh
curl -X PUT http://127.0.0.1:5984/new-base-tst
```
CouchDB rondra par :

```sh
{"ok":true}
```
L'extraction de la liste des bases de données montre encore une fois quelques résultats utiles:

```sh
curl -X GET http://127.0.0.1:5984/_all_dbs
```
CouchDB répondra par :

```sh
["_replicator","_users","lfd_blog","msys_blog","new-base-tst","repli_tst"]
`[``

Pour terminer, supprimons la deuxième base de données :

```sh
curl -X DELETE http://127.0.0.1:5984/bookstore
"bookstore"``,``"_users"``,``"blog"``,``"shopcart"``,``"test"``]`
```

To round things off, let’s delete the second database:
ebsesnew-base-tst
```

CouchDB répondra par :

```sh
`{``"ok"``:``true``}`
```
 liste des bases de données est maintenant la même qu'avant :

```sh
curl -X GET http://127.0.0.1:5984/_all_dbs
```

CouchDB répondra par :

```sh
["_replicator","_users","lfd_blog","msys_blog","repli_tst"]
```
Cela démontre la nature totalement "WEB" de la base CouchDB, les méthodes HTTP standard, GET, PUT, POST, et DELETE fonctionne parfaitement avec les URL approprié.

### 3.1  [Documents](http://guide.couchdb.org/editions/1/fr/documents.html)

Au cœur de CouchDB se trouve une structure de données appelée  _document_. Pour mieux comprendre et utiliser CouchDB, vous devez  _penser documents_. Ce chapitre vous guide à travers le cycle de vie des documents : de leur conception à leurs sauvegardes, nous passerons par leur consultation, leur agrégation et leur parcours au moyen des vues. Dans la section qui va suivre, vous verrez aussi comment CouchDB peut publier un document dans un autre format.

Les documents forment un ensemble de données autonomes. Vous avez peut-être déjà entendu parler du mot « tuple » pour indiquer quelque chose de semblable. Vos données sont souvent composées de types de données natifs tels que les entiers ou les chaînes de caractères. Les documents sont alors le premier niveau d’abstraction au-dessus de ces types de données natifs. Ils apportent une structure et regroupent de manière logique les éléments primitifs. Par exemple, la taille d’une personne peut être codée en tant qu’entier (`176`), mais cet entier fait souvent partie d’une structure plus vaste qui contient une étiquette (`"taille": 176`) et une donnée liée (`{"nom":"Chris", "taille": 176}`).

Le nombre d’éléments que vous stockez dans un document dépend avant tout de votre application, et aussi de la manière dont vous comptez utiliser les vues par la suite. En règle générale, un document correspond à peu de choses près à l’instance de l’objet correspondant dans votre langage de programmation. Dans le cas d’une boutique en ligne, vous aurez des  _objets_, des  _ventes_  et des  _commentaires_  pour vos marchandises. Ce sont des candidats potentiels pour devenir des objets et, par conséquent, des documents.

Les documents se différencient subtilement des objets traditionnels, car ils appartiennent à des auteurs et permettent les opérations CRUD (pour  _Create, Read, Update, Delete_, respectivement créer, lire, modifier, supprimer). Les logiciels de traitement de documents (comme votre logiciel de traitement de texte et votre tableur) ont conçu leur modèle de persistance des données autour du concept de document, ce qui permet aux auteurs de retrouver ce qu’ils ont écrit.

Les fonctions de validation sont là pour vous éviter d’avoir à vous soucier de l’impact de données erronées. Dans la plupart des cas, un logiciel gérant des documents laisse le soin à l’application de modifier et de manipuler les données avant de les sauvegarder à nouveau.

Prenons le cas suivant : vous permettez à vos utilisateurs de rédiger un commentaire sur l’objet « livre sympathique ». Vous avez la possibilité de stocker ces commentaires dans un tableau au sein même du document de cet objet. Il est ainsi très simple de retrouver les commentaires de l’objet, mais, comme on dit, ça ne résiste pas au passage à l’échelle. En effet, un objet populaire peut avoir des dizaines, des centaines, voire des milliers de commentaires.

Aussi, plutôt que de stocker une liste de commentaires au sein même de l’objet, il serait ici préférable de les regrouper dans un recueil (en anglais,  _collection_). CouchDB vous permet de parcourir aisément des recueils. Il est probable que vous désiriez n’en montrer qu’une dizaine ou une vingtaine à la fois et vouliez disposer de boutons « précédents » et « suivants ». En stockant individuellement les commentaires, vous pouvez les regrouper à l’aide des vues. Un groupe pourrait alors contenir tous les commentaires – ou des tranches de dix ou vingt –, le tout trié par rapport à l’objet auquel ils sont liés pour qu’il soit aisé de trouver le sous-ensemble dont vous avez besoin.

Par conséquent, une règle de base consiste à diviser en documents ce que vous gérerez individuellement dans votre application. Les objets sont unitaires, les commentaires sont unitaires, mais vous n’avez pas besoin de les redécouper. Les vues permettent de regrouper facilement vos documents de la manière qui vous intéresse.

Il est temps de concevoir notre application d’exemple pour vous montrer ce qu’il en est dans la pratique.



## 4\. Opérations HTTP standards


On reprend les opérations de base que nous avons exécutées dans le dernier chapitre, en essayant de comprendre les processus sous-jacents. Mais aussi  découvrir les mécanismes des fonctionnalités de l'interface "Futon" afin de pouvoir comprendre certains aspect d'arrière-plan afin de pouvoir les implémenter dans nos propre applications.

L'API peut être subdivisée en sections suivantes. Nous les explorerons individuellement:

1.  Server
2.  Databases
3.  Documents
4.  Replication
5.  Server

Celui-ci est basique et simple. Il peut servir à vérifier si CouchDB fonctionne bien. Il peut également servir de garde de sécurité pour les bibliothèques qui ont besoin d'une certaine version de CouchDB.
On utilise encore l'utilitaire Curl
```

To round things off, let’s delete the second database:
ebsesnew-basetst
```

CouchDB rpondra par :

```sh
`{``"o"``:``r``}`
```
 liste des bases de donnes est maintenant la mme quavant :

```sh
curl http://127.0.0.1:5984/
```

La réponses de CouchDB

```sh
{"couchdb":"Welcome","uuid":"303a0bd4194cef473c7a9c0badfeef11","version":"1.6.1","vendor":{"version":"1.6.1","name":"The Apache Software Foundation"}}
```
Nous récupérons une chaîne JSON, qui, si elle est analysée dans un objet natif ou une structure de données de notre langage de programmation, nous donne accès à la chaîne de caractères et aux informations de version.

Ce n'est pas très utile, mais cela illustre bien le comportement de CouchDB. Nous envoyons une requête HTTP et nous recevons en conséquence une chaîne JSON dans la réponse HTTP.

##### 4.1 Databases
Au sens strict, CouchDB est un SGBD Système de Gestion de Base de Données (DMS-Database Management System). Cela signifie qu'il peut contenir plusieurs bases de données. Une base de données est un espace qui contient des "données liées". Nous examinerons plus tard ce que cela signifie en détail. Dans la pratique, la terminologie se recoupe - on parle souvent d'un SGBD comme d'une "base de données" et d'une base de données au sein du SGBD comme d'une "base de données". Nous pourrions suivre cette petite bizarrerie, alors ne vous y trompez pas. En général, il devrait être clair à partir du contexte si nous parlons de l'ensemble de CouchDB ou d'une seule base de données au sein de CouchDB.

Maintenant, créons une nouvelle base !
Notez que nous utilisons maintenant à nouveau l'option -X pour dire à curl d'envoyer une requête PUT au lieu de la requête GET par défaut:

```sh
curl -X PUT http://127.0.0.1:5984/multimakers
```
Réponses de CouchDB:

```sh
{"ok":true}
```

Voilà, c'est ça. Nous avons créé une base de données et CouchDB nous a dit que tout s'était bien passé. Que se passe-t-il si nous essayons de créer une base de données qui existe déjà? Essayons de recréer cette base de données:

```sh
curl -X PUT http://127.0.0.1:5984/multimakers
```

Réponses de CouchDB:
```sh
{"error":"file_exists","reason":"The database could not be created, the file already exists."}
```
On récupère une erreur. C'est assez commode. CouchDB stocke chaque base de données dans un seul fichier.

Créez une autre base de données, cette fois avec l'option curl's -v (pour "verbose" le mode verbale). L'option verbale indique à Curl de nous montrer non seulement l'essentiel - le corps de la réponse HTTP - mais tous les détails sous-jacents de la requête et de la réponse:

Je vous propose de créer la base msystem-backup

```sh
curl -vX PUT http://philuser:monpass@127.0.0.1:5984/msystem-backup
```

La réponse de CouchDB en mode verbale :

```sh
\* Trying 127.0.0.1...  
\* TCP_NODELAY set  
\* Connected to 127.0.0.1 (127.0.0.1) port 5984 (#0)  
\* Server auth using Basic with user 'philuser'  
\> PUT /msystem-backup HTTP/1.6.1  
\> Host: 127.0.0.1:5984  
\> Authorization: Basic cGhpbHVzZXI6cUFHeDdRdmc4SmQ=  
\> User-Agent: curl/7.58.0  
\> Accept: */*  
>  
< HTTP/1.1 201 Created  
< Server: CouchDB/1.6.1 (Erlang OTP/17)  
< Location: http://127.0.0.1:5984/msystem-backup  
< Date: Sun, 11 Mar 2018 10:05:35 GMT  
< Content-Type: text/plain; charset=utf-8  
< Content-Length: 12  
< Cache-Control: must-revalidate  
<  
{"ok":true}  
\* Connection #0 to host 127.0.0.1 left intact
```

Une petite analyse section par section pour comprendre ce qui se passe et découvrir ce qui est important. Une fois que nous aurons vu cette sortie plusieurs fois, nous pourrons repérer les informations importantes plus facilement.

```sh
*   Trying 127.0.0.1...
* TCP_NODELAY set
* Connected to 127.0.0.1 (127.0.0.1) port 5984 (#0)
```
Au début Curl cherche a établir une connexion TCP sur l'adresse "127.0.0.1"
Ceci est curl nous indiquant qu'il va établir une connexion TCP au serveur CouchDB que nous avons spécifié dans notre requête URI. Pas du tout important, sauf pour déboguer les problèmes de réseautage.

```sh
`* Trying 127.0.0.1... connected * Connected to 127.0.0.1 (127.0.0.1) port 5984 (``#0)`
```
Curl nous dit qu'il s'est connecté avec succès à CouchDB. Encore une fois, pas important s'il n' y a pas de problème avec le réseau. Les lignes suivantes sont précédées de > et < caractères. signifie que la ligne a été envoyée à CouchDB textuellement (sans le > réel). < signifie que la ligne a été renvoyée en boucle par CouchDB.

```sh
 `> PUT /msystemstudent-backup HTTP/1.6.1`
```

CeciThis initie une requête HTTP. Saates an HTTP request. Its méethode est is PUT, l'URI est /msystem-backup, et lathe URI is /student-backup, and the HTTP version HTTP estis HTTP/1.1. Il y a aussi HTTP/1.0, ce qui est plus simple dans certains cas, mais pour toutes les raisons pratiques, nous devrions utiliser HTTP/1.1.

Ensuite, nous voyons un certainThere is also HTTP/1.0, which is simpler in some cases, but for all practical reasons We should be using HTTP/1.1.

Next, we see a noumbre d'en-têtes de requête. Ils sont utilisés pour fournir des détails supplémentaires sur la demande àer of request headers. These are used to provide additional details about the request to CouchDB.

```sh
`> User-Agent: curl/7.22.0 (x86_64-pc-linux-gnu) libcurl/7.22.0 OpenSSL/1.0.1 zlib/1.2.3.4 libidn/1.23 librtmp/2.3`
```

L'en-têtThe User-Agent indique à CouchDB quel logiciel client effectue la requête HTTP. C'est le programme de curling. Cet en-tête est souvheader tells CouchDB which piece of client software is doing the HTTP request. It’s the curl program. This header is oftent utile dans leseful in web déeveloppement web lorsqu'il y a des erreurs connues dans leshen there are known errors in client impléementations client pour lesquelles unthat a serveur pourrait vouloirmight want to préeparer la réponse. Il permet également de déterminer sur quelle the response for. It also helps to determine which plate-forme se trouve un utilisateur. Ces informations peuvent être utilisées pour des raisons techniques et statistiques. Pou a user is on. This information can be used for technical and statistical reasons. For CouchDB, l'en-têthe User-Agent n'est pas très pertineheader is not very relevant.


```sh
`> Host: 127.0.0.1:5984`
```

L'en-tête Host est requis par HTTP 1.1. Il indique auThe Host header is required by HTTP 1.1. It tells the serveur le nom d'hôte fourni avec lathe hostname that came with the requêteest.

```sh
`> Accept: */*`
```

L'en-tête Accepter indique àThe Accept header tells CouchDB quethat curl accepte n'importe quel type de média. Nous verrons pourquoi cela est utile un peu plus tards any media type. We’ll look into why this is useful a little later.

```sh
t`>`
```

UneAn empty ligne vide indique que les en-têtes de requête sont maintenant terminés et que le reste de ladenotes that the request headers are now finished and the rest of the requêteest contient des données que nous envoyons auains data we’re sending to the serveur. Dans ce cas, nous n'envoyons pas de données, donc le reste de la sortie de la boucle estIn this case, we’re not sending any data, so the rest of the curl output is déedié à la réponse HTTP.
cated to the HTTP response.

```sh
`< HTTP/1.1 201 Created`
```


La premièreThe first ligne de la réponse HTTP de CouchDB comprend l'information sur la version HTTP (encore une fois, pour confirmer que la version demandée pourrait être traitée), un code d'état HTTP et un message de code d'état. Des demandes dof CouchDB’s HTTP response includes the HTTP version information (again, to acknowledge that the requested version could be processed), an HTTP status code, and a status code message. Difféerentes déclenchent des codes de réponse différents. Il y en a toute une série qui disent au requests trigger different response codes. There’s a whole range of them telling the client (curl dans notrein our case) quelwhat effect lathe requête a eu sur lest had on the serveur. Our, si une erreur s'est produite, quel type d'if an error occurred, what kind of erreuor.

RFC 2616 (lathe HTTP 1.1 spéecification HTTP 1.1) déefinit un comportement clair poues clear behavior for lres codes de réponse. CouchDB suit entièrement le RFC. Le code de statut 201 Créé indique au client que laponse codes. CouchDB fully follows the RFC. The 201 Created status code tells the client that the ressource contre laquelle la dethe request was mande a été faite a été créée avec succès. Pas de surprise ici, mais si nous nous rappelons que nous avons reçu un message d'erreur lorsque nous avons essayé de créer cette base de données deux fois, nous savons maintenant que cette régainst was successfully created. No surprise here, but if we remember that we got an error message when we tried to create this database twice, we now know that this response pcourraitld inclurde un code de réponse différent.

Agir sur la base de réponses basées sur des codes de réponse est unea different response code.

Acting upon responses based on response codes is a common practique courantce. PaFor exeample, tous les codes de réponse de 400 (ou plus) nous informent qu'une erreur s'est produite. Si nous voulons raccourcir la logique et traiterall response codes of 400 (or greater than that) inform us that some error occurred. If we want to shortcut the logic and imméediatement l'ly deal with the erreuor, nous pourrions simplement vérifier un code de réponse >= 400.
we could just check a >= 400 response code.

```sh
`< Server: CouchDB/0.10.1 (Erlang OTP/R13B)`
```

L'en-tête du sThe Serveur est bon pour leheader is good for diagnostics. Il nous dit à quelle version de CouchDB et à quelle version d'Erlang sous-jacente nous parlonst tells us which CouchDB version and which underlying Erlang version we are talking to. EIn généeneral, on peut ignorer cet en-tête, mais il est bon de savoir qu'il est là si on en a besoin.
we can ignore this header, but it is good to know it’s there if We need it.

```sh
`< Date: Sun, 05 Jul 2009 22:48:28 GMT`
```

L'en-tête Date indique l'heure duThe Date header tells the time of the serveur. Comme le temps client et le temps serveur ne sont pas nécessairement synchronisés, cet en-tête estSince client and server time are not necessarily synchronized, this header is puremently informatif. Nous ne devrions pas construire une logique d'application critique en plus de cela !
Flo`{``"error"``:``"file_exists"``,``"reason"``:``"The database could not be created, the file already exists."``}`
```
On récupère une erreur. C'est assez commode. CouchDB stocke chaque base de données dans un seul fichier.

Créez une autre base de données, cette fois avec l'option curl's -v (pour "verbose"). L'option verbale indique à Curl de nous montrer non seulement l'essentiel - le corps de la réponse HTTP - mais tous les détails sous-jacents de la requête et de la réponse:

```sh
curl -vX PUT http://127.0.0.1:5984/student-backup
`curl -X GET  [http://127.0.0.1:5984/_all_dbs](http://127.0.0.1:5984/_all_dbs)`
```

CouchDB will respond with:

```sh
`[``"_users"``,``"blog"``,``"shopcart"``,``"test"``]`
```

Everything is done using the standard HTTP methods, GET, PUT, POST, and DELETE with the appropriate URI.

### 3.1  [Documents](http://guide.couchdb.org/draft/documents.html#documents)

_Documents_ are CouchDB’s central data structure. To better understand and use CouchDB, we need to  _think in terms of documents_. In this chapter we will walk though the lifecycle of designing and saving a document. We’ll follow up by reading documents and aggregating and querying them with views.

Documents are self-contained units of data. The data is usually made up of small native types such as integers and strings. Documents are the first level of abstraction over these native types. They provide some structure and logically group the primitive data. The height of a person might be encoded as an integer (_176_), but this integer is usually part of a larger structure that contains a label (_“height”: 176_) and related data (_{“name”:”Chris”, “height”: 176}_).

How many data items can be put into the documents depends on the application and a bit on how we want to use views. Generally, a document corresponds to an object instance in the programming language.

Documents differ subtly from garden-variety objects in that they usually have authors and CRUD operations (create, read, update, delete). Document-based software (like the word processors and spreadsheets) build their storage model around saving documents so that authors get back what they created.

Validation functions are available so that we don’t have to worry about bad data causing errors in our system. Often in document-based software, the client application edits and manipulates the data, saving it back.

Let’s suppose a user can comment on the item (“lovely book”); we have the option to store the comments as an array, on the item document. This makes it trivial to find the item’s comments, but, as they say, “it doesn’t scale.” A popular item could have tens of comments, or even hundreds or more.

Instead of storing a list on the item document, in this case it may be acutally better to model comments into a collection of documents. There are patterns for accessing collections, which CouchDB makes easy. We likely want to show only 10 or 20 at a time and provide  _previous_ and  _next_ links. By handling comments as individual entities, we can group them with views. A group could be the entire collection or slices of 10 or 20, sorted by the item they apply to so that it’s easy to grab the set we need.

Everything that will be handled separately in the application should be broken up into documents. Items are single, and comments are single, but we don’t need to break them into smaller pieces. Views provide a convenient way to group our documents in meaningful ways.

## 4\. Common HTTP operations

We start out by revisiting the basic operations we ran in the last chapter, looking behind the scenes. We will also discover what Futon runs in the background in order to give us the nice features we saw earlier.

While explaining the API bits and pieces, we sometimes need to take a larger detour to explain the reasoning for a particular request. This is a good opportunity for us to tell why CouchDB works the way it does.

The API can be subdivided into the following sections. We’ll explore them individually:

1.  Server
2.  Databases
3.  Documents
4.  Replication
5.  Server

This one is basic and simple. It can serve as a sanity check to see if CouchDB is running at all. It can also act as a safety guard for libraries that require a certain version of CouchDB. We’re using the curl utility again:

```sh
`curl  [http://127.0.0.1:5984/](http://127.0.0.1:5984/)`
```

CouchDB replies, all excited to get going:

```sh
`{``"couchdb"``:``"Welcome"``,``"version"``:``"1.0.1"``}`
```

We get back a JSON string, which, if parsed into a native object or data structure of our programming language, gives us access to the welcome string and version information.

This is not terribly useful, but it illustrates nicely the way CouchDB behaves. We send an HTTP request and we receive a JSON string in the HTTP response as a result.

### 4.1 Databases

Strictly speaking, CouchDB is a database management system (DMS). That means it can hold multiple databases. A database is a bucket that holds “related data.” We’ll explore later what that means in detail. In practice, the terminology is overlapping—often people refer to a DMS as “a database” and also a database within the DMS as “a database.” We might follow that slight oddity, so don’t get confused by it. In general, it should be clear from the context if we are talking about the whole of CouchDB or a single database within CouchDB.

Now let’s make one! Note that we’re now using the -X option again to tell curl to send a PUT request instead of the default GET request:

```sh
`curl -X PUT  [http://127.0.0.1:5984/student](http://127.0.0.1:5984/student)`
```

CouchDB replies:

```sh
`{``"ok"``:``true``}`
```

That’s it. We created a database and CouchDB told us that all went well. What happens if we try to create a database that already exists? Let’s try to create that database again:

```sh
`curl -X PUT  [http://127.0.0.1:5984/student](http://127.0.0.1:5984/student)`
```

CouchDB replies:

```sh
`{``"error"``:``"file_exists"``,``"reason"``:``"The database could not be created, the file already exists."``}`
```

We get back an error. This is pretty convenient. CouchDB stores each database in a single file.

Let’s create another database, this time with curl’s -v (for “verbose”) option. The verbose option tells curl to show us not only the essentials—the HTTP response body—but all the underlying request and response details:

```sh
`curl -vX PUT  [http://127.0.0.1:5984/student-backup](http://127.0.0.1:5984/student-backup)`
```

Curl élabore:elaborates:

```sh
`* About to connect() to 127.0.0.1 port 5984 (``#0)`

`02`

`03`

`* Trying 127.0.0.1... connected`

`04`

`05`

`> PUT /student-backup HTTP/1.1`

`06`

`07`

`> User-Agent: curl/7.22.0 (x86_64-pc-linux-gnu) libcurl/7.22.0 OpenSSL/1.0.1 zlib/1.2.3.4 libidn/1.23 librtmp/2.3`

`08`

`09`

`> Host: 127.0.0.1:5984`

`10`

`11`

`> Accept: */*`

`12`

`13`

`>`

`14`

`15`

`< HTTP/1.1 201 Created`

`16`

`17`

`< Server: CouchDB/1.0.1 (Erlang OTP/R14B)`

`18`

`19`

`< Location:  [http://127.0.0.1:5984/student-backup](http://127.0.0.1:5984/student-backup)`

`20`

`21`

`< Date: Sat, 15 Feb 2014 17:50:51 GMT`

`22`

`23`

`< Content-Type: text/plain;charset=utf-8`

`24`

`25`

`< Content-Length: 12`

`26`

`27`

`< Cache-Control: must-revalidate`

`28`

`29`

`< {``"ok"``:``true``} * Connection` `#0 to host 127.0.0.1 left intact * Closing connection #0`
```

Let’s step through this line by line in order to understand what’s going on and find out what’s important. Once we’ve seen this output a few times, we’ll be able to spot the important bits more easily.

```sh
`* About to connect() to 127.0.0.1 port 5984 (``#0)`
```

This is curl telling us that it is going to establish a TCP connection to the CouchDB server we specified in our request URI. Not at all important, except when debugging networking issues.

```sh
`* Trying 127.0.0.1... connected * Connected to 127.0.0.1 (127.0.0.1) port 5984 (``#0)`
```

Curl tells us it successfully connected to CouchDB. Again, not important if there is no problem with the network. The following lines are prefixed with > and < characters. > means the line was sent to CouchDB verbatim (without the actual >). < means the line was sent back to curl by CouchDB.

```sh
`> PUT /student-backup HTTP/1.1`
```

This initiates an HTTP request. Its method is PUT, the URI is /student-backup, and the HTTP version is HTTP/1.1. There is also HTTP/1.0, which is simpler in some cases, but for all practical reasons We should be using HTTP/1.1.

Next, we see a number of request headers. These are used to provide additional details about the request to CouchDB.

```sh
`> User-Agent: curl/7.22.0 (x86_64-pc-linux-gnu) libcurl/7.22.0 OpenSSL/1.0.1 zlib/1.2.3.4 libidn/1.23 librtmp/2.3`
```

The User-Agent header tells CouchDB which piece of client software is doing the HTTP request. It’s the curl program. This header is often useful in web development when there are known errors in client implementations that a server might want to prepare the response for. It also helps to determine which platform a user is on. This information can be used for technical and statistical reasons. For CouchDB, the User-Agent header is not very relevant.

```sh
`> Host: 127.0.0.1:5984`
```

The Host header is required by HTTP 1.1. It tells the server the hostname that came with the request.

```sh
`> Accept: */*`
```

The Accept header tells CouchDB that curl accepts any media type. We’ll look into why this is useful a little later.

```sh
`>`
```

An empty line denotes that the request headers are now finished and the rest of the request contains data we’re sending to the server. In this case, we’re not sending any data, so the rest of the curl output is dedicated to the HTTP response.

```sh
`< HTTP/1.1 201 Created`
```

The first line of CouchDB’s HTTP response includes the HTTP version information (again, to acknowledge that the requested version could be processed), an HTTP status code, and a status code message. Different requests trigger different response codes. There’s a whole range of them telling the client (curl in our case) what effect the request had on the server. Or, if an error occurred, what kind of error.

RFC 2616 (the HTTP 1.1 specification) defines clear behavior for response codes. CouchDB fully follows the RFC. The 201 Created status code tells the client that the resource the request was made against was successfully created. No surprise here, but if we remember that we got an error message when we tried to create this database twice, we now know that this response could include a different response code.

Acting upon responses based on response codes is a common practice. For example, all response codes of 400 (or greater than that) inform us that some error occurred. If we want to shortcut the logic and immediately deal with the error, we could just check a >= 400 response code.

```sh
`< Server: CouchDB/0.10.1 (Erlang OTP/R13B)`
```

The Server header is good for diagnostics. It tells us which CouchDB version and which underlying Erlang version we are talking to. In general, we can ignore this header, but it is good to know it’s there if We need it.

```sh
`< Date: Sun, 05 Jul 2009 22:48:28 GMT`
```

The Date header tells the time of the server. Since client and server time are not necessarily synchronized, this header is purely informational. We shouldn’t build any critical application logic on top of this!
> PUT /msystem-backup HTTP/1.6.1
```

Ceci initie une requête HTTP. Sa méthode est PUT, l'URI est /msystem-backup, et la version HTTP est HTTP/1.1. Il y a aussi HTTP/1.0, ce qui est plus simple dans certains cas, mais pour toutes les raisons pratiques, nous devrions utiliser HTTP/1.1.

Ensuite, nous voyons un certain nombre d'en-têtes de requête. Ils sont utilisés pour fournir des détails supplémentaires sur la demande à CouchDB.

```sh
`> User-Agent: curl/7.22.0 (x86_64-pc-linux-gnu) libcurl/7.22.0 OpenSSL/1.0.1 zlib/1.2.3.4 libidn/1.23 librtmp/2.3`
```

L'en-tête User-Agent indique à CouchDB quel logiciel client effectue la requête HTTP. C'est le programme de curling. Cet en-tête est souvent utile dans le développement web lorsqu'il y a des erreurs connues dans les implémentations client pour lesquelles un serveur pourrait vouloir préparer la réponse. Il permet également de déterminer sur quelle plate-forme se trouve un utilisateur. Ces informations peuvent être utilisées pour des raisons techniques et statistiques. Pour CouchDB, l'en-tête User-Agent n'est pas très pertinent.


```sh
`> Host: 127.0.0.1:5984`
```

L'en-tête Host est requis par HTTP 1.1. Il indique au serveur le nom d'hôte fourni avec la requête.

```sh
`> Accept: */*`
```

L'en-tête Accepter indique à CouchDB que curl accepte n'importe quel type de média. Nous verrons pourquoi cela est utile un peu plus tard.

```sh
t`>`
```

Une ligne vide indique que les en-têtes de requête sont maintenant terminés et que le reste de la requête contient des données que nous envoyons au serveur. Dans ce cas, nous n'envoyons pas de données, donc le reste de la sortie de la boucle est dédié à la réponse HTTP.


```sh
`< HTTP/1.1 201 Created`
```


La première ligne de la réponse HTTP de CouchDB comprend l'information sur la version HTTP (encore une fois, pour confirmer que la version demandée pourrait être traitée), un code d'état HTTP et un message de code d'état. Des demandes différentes déclenchent des codes de réponse différents. Il y en a toute une série qui disent au client (curl dans notre cas) quel effet la requête a eu sur le serveur. Ou, si une erreur s'est produite, quel type d'erreur.

RFC 2616 (la spécification HTTP 1.1) définit un comportement clair pour les codes de réponse. CouchDB suit entièrement le RFC. Le code de statut 201 Créé indique au client que la ressource contre laquelle la demande a été faite a été créée avec succès. Pas de surprise ici, mais si nous nous rappelons que nous avons reçu un message d'erreur lorsque nous avons essayé de créer cette base de données deux fois, nous savons maintenant que cette réponse pourrait inclure un code de réponse différent.

Agir sur la base de réponses basées sur des codes de réponse est une pratique courante. Par exemple, tous les codes de réponse de 400 (ou plus) nous informent qu'une erreur s'est produite. Si nous voulons raccourcir la logique et traiter immédiatement l'erreur, nous pourrions simplement vérifier un code de réponse >= 400.


```sh
`< Server: CouchDB/0.10.1 (Erlang OTP/R13B)`
```

L'en-tête du serveur est bon pour le diagnostic. Il nous dit à quelle version de CouchDB et à quelle version d'Erlang sous-jacente nous parlons. En général, on peut ignorer cet en-tête, mais il est bon de savoir qu'il est là si on en a besoin.


```sh
`< Date: Sun, 05 Jul 2009 22:48:28 GMT`
```

L'en-tête Date indique l'heure du serveur. Comme le temps client et le temps serveur ne sont pas nécessairement synchronisés, cet en-tête est purement informatif. Nous ne devrions pas construire une logique d'application critique en plus de cela !
Flo`> PUT /student-backup HTTP/1.1`
```

This initiates an HTTP request. Its method is PUT, the URI is /student-backup, and the HTTP version is HTTP/1.1. There is also HTTP/1.0, which is simpler in some cases, but for all practical reasons We should be using HTTP/1.1.

Next, we see a number of request headers. These are used to provide additional details about the request to CouchDB.

```sh
`> User-Agent: curl/7.22.0 (x86_64-pc-linux-gnu) libcurl/7.22.0 OpenSSL/1.0.1 zlib/1.2.3.4 libidn/1.23 librtmp/2.3`
```

The User-Agent header tells CouchDB which piece of client software is doing the HTTP request. It’s the curl program. This header is often useful in web development when there are known errors in client implementations that a server might want to prepare the response for. It also helps to determine which platform a user is on. This information can be used for technical and statistical reasons. For CouchDB, the User-Agent header is not very relevant.

```sh
`> Host: 127.0.0.1:5984`
```

The Host header is required by HTTP 1.1. It tells the server the hostname that came with the request.

```sh
`> Accept: */*`
```

The Accept header tells CouchDB that curl accepts any media type. We’ll look into why this is useful a little later.

```sh
`>`
```

An empty line denotes that the request headers are now finished and the rest of the request contains data we’re sending to the server. In this case, we’re not sending any data, so the rest of the curl output is dedicated to the HTTP response.

```sh
`< HTTP/1.1 201 Created`
```

The first line of CouchDB’s HTTP response includes the HTTP version information (again, to acknowledge that the requested version could be processed), an HTTP status code, and a status code message. Different requests trigger different response codes. There’s a whole range of them telling the client (curl in our case) what effect the request had on the server. Or, if an error occurred, what kind of error.

RFC 2616 (the HTTP 1.1 specification) defines clear behavior for response codes. CouchDB fully follows the RFC. The 201 Created status code tells the client that the resource the request was made against was successfully created. No surprise here, but if we remember that we got an error message when we tried to create this database twice, we now know that this response could include a different response code.

Acting upon responses based on response codes is a common practice. For example, all response codes of 400 (or greater than that) inform us that some error occurred. If we want to shortcut the logic and immediately deal with the error, we could just check a >= 400 response code.

```sh
`< Server: CouchDB/0.10.1 (Erlang OTP/R13B)`
```

The Server header is good for diagnostics. It tells us which CouchDB version and which underlying Erlang version we are talking to. In general, we can ignore this header, but it is good to know it’s there if We need it.

```sh
`< Date: Sun, 05 Jul 2009 22:48:28 GMT`
```

The Date header tells the time of the server. Since client and server time are not necessarily synchronized, this header is purely informational. We shouldn’t build any critical application logic on top of this!

```sh
`< Content-Type: text/plain;charset=utf-8`
```

The Content-Type header tells which MIME type the HTTP response body uses and what encoding is used for it. We already know that CouchDB returns JSON strings. The appropriate Content-Type header is application/json. Why do we see text/plain? This is where pragmatism wins over purity. Sending an application/json Content-Type header will make a browser offer the returned JSON for download instead of just displaying it. Since it is extremely useful to be able to test CouchDB from a browser, CouchDB sends a text/plain content type, so all browsers will display the JSON as text.

There are some extensions that make our browser JSON-aware, but they are not installed by default. For more information, look at the popular JSONView extension, available for both Firefox and Chrome.

If we send Accept: application/json in our request, CouchDB knows that we can deal with a pure JSON response with the proper Content-Type header and will use it instead of text/plain.

```sh
`< Content-Length: 12`
```

The Content-Length header simply tells us how many bytes the response body has.

```sh
`< Cache-Control: must-revalidate`
```

This Cache-Control header tells us, or any proxy server between CouchDB and us, not to cache this response.

```sh
`<`
```

This empty line tells us we’re done with the response headers and what follows now is the response body.

```sh
`* Connection` `#0 to host 127.0.0.1 left intact * Closing connection #0`
```

The last two lines are curl telling us that it kept the TCP connection it opened in the beginning open for a moment, but then closed it after it received the entire response.

```sh
`> curl -vX DELETE  [http://127.0.0.1:5984/albums-backup](http://127.0.0.1:5984/albums-backup)`
```

This deletes a CouchDB database. The request will remove the file that the database contents are stored in. We need to use this command with care, as our data will be deleted without a chance to bring it back easily if we don’t have a backup copy.

The console will look like this:

```sh
`* About to connect() to 127.0.0.1 port 5984 (``#0)`

`02`

`03`

`* Trying 127.0.0.1... connected`

`04`

`05`

`> DELETE /student-backup HTTP/1.1`

`06`

`07`

`> User-Agent: curl/7.22.0 (x86_64-pc-linux-gnu) libcurl/7.22.0 OpenSSL/1.0.1 zlib/1.2.3.4 libidn/1.23 librtmp/2.3`

`08`

`09`

`> Host: 127.0.0.1:5984`

`10`

`11`

`> Accept: */*`

`12`

`13`

`>`

`14`

`15`

`< HTTP/1.1 200 OK`

`16`

`17`

`< Server: CouchDB/1.0.1 (Erlang OTP/R14B)`

`18`

`19`

`< Date: Sat, 15 Feb 2014 17:53:58 GMT`

`20`

`21`

`< Content-Type: text/plain;charset=utf-8`

`22`

`23`

`< Content-Length: 12`

`24`

`25`

`< Cache-Control: must-revalidate`

`26`

`27`

`<`

`28`

`29`

`{``"ok"``:``true``}`

`30`

`31`

`* Connection` `#0 to host 127.0.0.1 left intact`

`32`

`33`

`* Closing connection` `#0`
```

This section went knee-deep into HTTP and set the stage for discussing the rest of the core CouchDB API. Next stop: documents.

### 4.2 Documents

Let’s have a closer look at our document creation requests with the curl -v flag that was helpful when we explored the database API earlier. This is also a good opportunity to create more documents that we can use in later examples.

We’ll add some more of our favorite music albums. Get a fresh UUID from the /_uuids resource. If we don’t remember how that works, w can look it up a few pages back.

```sh
`curl -vX PUT  [http://127.0.0.1:5984/albums/70b50bfa0a4b3aed1f8aff9e92dc16a0](http://127.0.0.1:5984/albums/70b50bfa0a4b3aed1f8aff9e92dc16a0)  -d` `'{"title":"Blackened Sky","artist":"Biffy Clyro","year":2002}'`
```

Now with the -v option, CouchDB’s reply (with only the important bits shown) looks like this:

```sh
`> PUT /albums/70b50bfa0a4b3aed1f8aff9e92dc16a0 HTTP/1.1`

`2`

`>`

`3`

`< HTTP/1.1 201 Created`

`4`

`< Location:  [http://127.0.0.1:5984/albums/70b50bfa0a4b3aed1f8aff9e92dc16a0](http://127.0.0.1:5984/albums/70b50bfa0a4b3aed1f8aff9e92dc16a0)`

`5`

`< Etag:` `"1-2248288203"`

`6`

`<`

`7`

`{``"ok"``:``true``,``"id"``:``"70b50bfa0a4b3aed1f8aff9e92dc16a0"``,``"rev"``:``"1-2248288203"``}`
```

We’re getting back the 201 Created HTTP status code in the response headers, as we saw earlier when we created a database. The Location header gives us a full URL to our newly created document and there’s a new header. An Etag in HTTP-speak identifies a specific version of a resource. In this case, it identifies a specific version (the first one) of our new document. An Etag is the same as a CouchDB document revision number, and it shouldn’t come as a surprise that CouchDB uses revision numbers for Etags. Etags are useful for caching infrastructures.

### 4.3 Attachments

CouchDB documents can have attachments just like an email message can have attachments. An attachment is identified by a name and includes its MIME type (or Content-Type) and the number of bytes the attachment contains. Attachments can consist of any type of data. It is easier to think about attachments as files attached to a document. These files can be text, images, Word documents, music, or movie files. Let’s make one.

Attachments get their own URL where we can upload data. Let’s suppose we want to add the album artwork to the 6e1295ed6c29495e54cc05947f18c8af document (“There is Nothing Left to Lose”), and let’s also say the artwork is in a file artwork.jpg in the current directory:

```sh
`> curl -vX PUT  [http://127.0.0.1:5984/albums/6e1295ed6c29495e54cc05947f18c8af/artwork.jpg?rev=2-2739352689](http://127.0.0.1:5984/albums/6e1295ed6c29495e54cc05947f18c8af/artwork.jpg?rev=2-2739352689)  --data-binary @artwork.jpg -H` `"Content-Type: image/jpg"`
```

The –data-binary @ option tells curl to read a file’s contents into the HTTP request body. We’re using the -H option to tell CouchDB that we’re uploading a JPEG file. CouchDB will keep this information around and will send the appropriate header when requesting this attachment; in case of an image like this, a browser will render the image instead of offering the data for download. This will come in handy later. Note that we need to provide the current revision number of the document we’re attaching the artwork to, just as if we would update the document.

If we request the document again, we will see a new member:

```sh
`curl  [http://127.0.0.1:5984/albums/6e1295ed6c29495e54cc05947f18c8af](http://127.0.0.1:5984/albums/6e1295ed6c29495e54cc05947f18c8af)`
```

CouchDB replies:

```sh
`{``"_id"``:``"6e1295ed6c29495e54cc05947f18c8af"``,``"_rev"``:``"3-131533518"``,``"title"``:` `"There is Nothing Left to Lose"``,``"artist"``:``"Foo Fighters"``,``"year"``:``"1997"``,``"_attachments"``:{``"artwork.jpg"``:{``"stub"``:``true``,``"content_type"``:``"image/jpg"``,``"length"``:52450}}}`
```

_attachments is a list of keys and values where the values are JSON objects containing the attachment metadata. stub=true tells us that this entry is just the metadata. If we use the ?attachments=true HTTP option when requesting this document, we’d get a Base64-encoded string containing the attachment data.

We’ll have a look at more document request options later as we explore more features of CouchDB, such as replication, which is the next topic.

### 4.4 Replication

CouchDB replication is a mechanism to synchronize databases. Much like rsync synchronizes two directories locally or over a network, replication synchronizes two databases locally or remotely.

Using a simple POST request, we tell CouchDB the source and the target of a replication and CouchDB will figure out which documents and new document revisions exist on the source DB and they are not yet on the target DB, and will proceed to move the missing documents and revisions over.

First, we’ll create a target database. Note that CouchDB won’t automatically create a target database and will return a replication failure if the target doesn’t exist:

```sh
`curl -X PUT  [http://127.0.0.1:5984/albums-replica](http://127.0.0.1:5984/albums-replica)`
```

Now we can use the database albums-replica as a replication target:

```sh
`curl -vX POST  [http://127.0.0.1:5984/_replicate](http://127.0.0.1:5984/_replicate)  -d` `'{"source":"albums","target":"albums-replica"}'`  `-H` `"Content-Type: application/json"`
```

CouchDB replies (this time we formatted the output so We can read it more easily):

```sh
`{`

`02`

`"history"``: [`

`03`

`{`

`04`

`"start_last_seq"``: 0,`

`05`

`"missing_found"``: 2,`

`06`

`"docs_read"``: 2,`

`07`

`"end_last_seq"``: 5,`

`08`

`"missing_checked"``: 2,`

`09`

`"docs_written"``: 2,`

`10`

`"doc_write_failures"``: 0,`

`11`

`"end_time"``:` `"Sat, 11 Jul 2009 17:36:21 GMT"``,`

`12`

`"start_time"``:` `"Sat, 11 Jul 2009 17:36:20 GMT"`

`13`

`}`

`14`

`],`

`15`

`"source_last_seq"``: 5,`

`16`

`"session_id"``:` `"924e75e914392343de89c99d29d06671"``,`

`17`

`"ok"``:` `true`

`18`

`}`
```

CouchDB maintains a session history of replications. The response for a replication request contains the history entry for this replication session. It is also worth noting that the request for replication will stay open until replication closes. If we have a lot of documents, it’ll take a while until they are all replicated and we won’t get back the replication response until all documents are replicated. It is important to note that replication replicates the database only as it was at the point in time when replication was started. So, any additions, modifications, or deletions subsequent to the start of replication will not be replicated.

We’ll punt on the details again: the “ok”: true at the end tells us all went well. If we have a look at the albums-replica database, we should see all the documents that we created in the albums database.

In CouchDB terms, we created a local copy of a database. This is useful for backups or to keep snapshots of a specific state of data around for later.

There are more types of replication useful in other situations. The source and target members of our replication request are actually links (like in HTML) and so far we’ve seen links relative to the server we’re working on (hence local). We can also specify a remote database as the target:

```sh
`curl -vX POST  [http://127.0.0.1:5984/_replicate](http://127.0.0.1:5984/_replicate)  -d` `'{"source":"albums","target":"[http://example.org:5984/albums-replica](http://example.org:5984/albums-replica)"}'`  `-H` `"Content-Type: application/json"`
```

Using a local source and a remote target database is called push replication. We’re pushing changes to a remote server.

We can also use a remote source and a local target to do a pull replication. This is great for getting the latest changes from a server that is used by others:


```sh
`curl -vX POST  [http://127.0.0.1:5984/_replicate](http://127.0.0.1:5984/_replicate)  -d` `'{"source":"[http://example.org:5984/albums-replica](http://example.org:5984/albums-replica)","target":"albums"}'`  `-H` `"Content-Type: application/json"`
```

Finally, we can run remote replication, which is mostly useful for management operations:

```sh
`curl -vX POST  [http://127.0.0.1:5984/_replicate](http://127.0.0.1:5984/_replicate)  -d` `'{"source":"[http://example.org:5984/albums](http://example.org:5984/albums)","target":"[http://example.org:5984/albums-replica](http://example.org:5984/albums-replica)"}'`  `-H` `"Content-Type: application/json"`
```

## 5\. JSON

CouchDB uses JavaScript Object Notation (JSON) for data storage. JSON is a lightweight format based on a subset of JavaScript syntax. One of the best bits about JSON is that it’s easy to read and write by hand, much more so than something like XML. We can parse it naturally with JavaScript because it shares part of the same syntax. This really comes in handy when we’re building dynamic web applications and we want to fetch some data from the server.

Here’s a sample JSON document:

```sh
`{`

`02`

`"Subject"``:` `"I like Plankton"``,`

`03`

`"Author"``:` `"Rusty"``,`

`04`

`"PostedDate"``:` `"2006-08-15T17:30:12-04:00"``,`

`05`

`"Tags"``: [`

`06`

`"plankton"``,`

`07`

`"baseball"``,`

`08`

`"decisions"`

`09`

`],`

`10`

`"Body"``:` `"I decided today that I don't like baseball. I like plankton."`

`11`

`}`
```

We can see that the general structure is based around key/value pairs and lists of things.

### 5.1 Data Types

JSON has a number of basic data types We can use. We’ll cover them all here.

### 5.2 Numbers

We can have positive integers: “Count”: 253

Or negative integers: “Score”: -19

Or floating-point numbers: “Area”: 456.31

Or scientific notation: “Density”: 5.6e+24

### 5.3 Strings

We can use strings for values:

```sh
`"Author"``:` `"Rusty"`
```

We have to escape some special characters, like tabs or newlines:

```sh
`"poem"``:` `"May I compare thee to some\\n\\tsalty plankton."`
```

The  [JSON site](http://www.json.org/)  has details on what needs to be escaped.

### 5.4 Booleans

We can have boolean true values:

```sh
`"Draft"``:` `true`
```

Or boolean false values:

```sh
`"Draft"``:` `false`
```

**Arrays**

An array is a list of values:

```sh
`"Tags"``: [``"plankton"``,` `"baseball"``,` `"decisions"``]`
```

An array can contain any other data type, including arrays:

```sh
`"Context"``: [``"dog"``, [1,` `true``], {``"Location"``:` `"puddle"``}]`
```

**Objects**

An object is a list of key/value pairs:

```sh
`{``"Subject"``:` `"I like Plankton"``,` `"Author"``:` `"Rusty"``}`
```

**Nulls**

We can have null values:

```sh
`"Surname"``: null`
```

## 6\. Documents

Documents are CouchDB’s central data structure. The idea behind a document is, unsurprisingly, that of a real-world document – a sheet of paper such as an invoice, a recipe, or a business card. We have already learned that CouchDB uses the JSON format to store documents. Let’s see how this storing works at the lowest level.

Each document in CouchDB has an ID. This ID is unique per database. We are free to choose any string to be the ID, but for best results we recommend a UUID (or GUID), i.e., a Universally (or Globally) Unique IDentifier. UUIDs are random numbers that have such a low collision probability. We can make thousands of UUIDs per minute for millions of years without ever creating a duplicate. This is a great way to ensure two independent people cannot create two different documents with the same ID. Why should we care what somebody else is doing? For one, somebody else could be at a later time or on a different computer; secondly, CouchDB replication let’s us share documents with others and using UUIDs ensures that it all works. But more on that later; let’s make some documents:

```sh
`curl -X PUT  [http://127.0.0.1:5984/shopcart/6e1295ed6c29495e54cc05947f18c8af](http://127.0.0.1:5984/shopcart/6e1295ed6c29495e54cc05947f18c8af)  -d` `'{"title":"There is Nothing Left to Lose","artist":"Foo Fighters"}'`
```

CouchDB replies:

```sh
`{``"ok"``:``true``,``"id"``:``"6e1295ed6c29495e54cc05947f18c8af"``,``"rev"``:``"1-2902191555"``}`
```

The curl command appears complex, but let’s break it down. First, -X PUT tells curl to make a PUT request. It is followed by the URL that specifies Wer CouchDB IP address and port. The resource part of the URL /albums/6e1295ed6c29495e54cc05947f18c8af specifies the location of a document inside our albums database. The wild collection of numbers and characters is a UUID. This UUID is Wer document’s ID. Finally, the -d flag tells curl to use the following string as the body for the PUT request. The string is a simple JSON structure including title and artist attributes with their respective values.

A CouchDB document is simply a JSON object. We can use any JSON structure with nesting. We can fetch the document’s revision information by adding ?revs_info=true to the get request.

To get a UUID, we use:

```sh
`curl -X GET  [http://127.0.0.1:5984/_uuids](http://127.0.0.1:5984/_uuids)`
```

CouchDb will reply us back, like this:

```sh
`{``"uuids"``:[``"6e1295ed6c29495e54cc05947f18c8af"``]}`
```

Here are two simple examples of documents:

```sh
t`{`

`02`

`"_id"``:``"discussion_tables"``,`

`03`

`"_rev"``:``"D1C946B7"``,`

`04`

`"Sunrise"``:``true``,`

`05`

`"Sunset"``:``false``,`

`06`

`"FullHours"``:[1,2,3,4,5,6,7,8,9,10],`

`07`

`"Activities"``: [`

`08`

`{``"Name"``:``"Football"``,` `"Duration"``:2,` `"DurationUnit"``:``"Hours"``},`

`09`

`{``"Name"``:``"Breakfast"``,` `"Duration"``:40,` `"DurationUnit"``:``"Minutes"``,` `"Attendees"``:[``"Jan"``,` `"Damien"``,` `"Laura"``,` `"Gwendolyn"``,` `"Roseanna"``]}`

`10`

`]`

`11`

`}`

`1`

`{`

`2`

`"_id"``:``"some_doc_id"``,`

`3`

`"_rev"``:``"D1C946B7"``,`

`4`

`"Subject"``:``"I like Plankton"``,`

`5`

`"Author"``:``"Rusty"``,`

`6`

`"PostedDate"``:``"2006-08-15T17:30:12-04:00"``,`

`7`

`"Tags"``:[``"plankton"``,` `"baseball"``,` `"decisions"``],`

`8`

`"Body"``:``"I decided today that I don't like baseball. I like plankton."`

`9`

`}`
```

### 6.1 Special Fields

Note that any top-level fields within a JSON document containing a name that starts with a _ prefix are reserved for use by CouchDB itself. Also see Reserved_words. Currently (0.10+) reserved fields are:

**Field Name**

**Description**

__id_

The unique identifier of the document (**mandatory** and  **immutable**)

__rev_

The current MVCC-token/revision of this document (**mandatory** and  **immutable**)

__attachments_

If the document has attachments, _attachments holds a (meta-)data structure (see section on  [HTTP\_Document\_API#Attachments](http://wiki.apache.org/couchdb/HTTP_Document_API#Attachments))

__deleted_

Indicates that this document has been deleted and previous revisions will be removed on next compaction run

__revisions_

Revision history of the document

_\_revs\_info_

A list of revisions of the document, and their availability

__conflicts_

Information about conflicts

_\_deleted\_conflicts_

Information about conflicts

_\_local\_seq_

Sequence number of the revision in the database (as found in the _changes feed)

Table 1

To request a special field to be returned along with the normal fields we get when we request a document, add the desired field as a query parameter without the leading underscore in a GET request:

```sh
`curl -X GET` `'[http://localhost:5984/my_database/my_document?conflicts=true](http://localhost:5984/my_database/my_document?conflicts=true)'`
```

This request will return a document that includes the special field ‘\_conflicts’ which contains all the conflicting revisions of “my\_document”.

\[**Exception:** The query parameter for the __revisions_ special field is ‘revs’, not ‘revisions’.\]

### 6.2 Document IDs

Document IDs don’t have restrictions on what characters can be used. Although it should work, it is recommended to use non-special characters for document IDs. By using special characters, we have to be aware of proper URL en-/decoding. Documents prefixed with  ___  are special documents:

**Document ID prefix**

**Description**

__design/_

are  [DesignDocuments](http://wiki.apache.org/couchdb/DesignDocuments)

__local/_

are not being replicated (local documents) and used for  [Replication](http://wiki.apache.org/couchdb/Replication)  checkpointing.

Table 2

We can have / as part of the document ID but if We refer to a document in a URL We must always encode it as %2F. One special case is _design/ documents, those accept either / or %2F for the / after  __design_, although / is preferred and %2F is still needed for the rest of the DocID.

### 6.3 Working With Documents Over HTTP

**GET**

To retrieve a document, simply perform a  _GET_  operation at the document’s URL:

```sh
`curl -X GET  [http://127.0.0.1:5984/shopcart/6e1295ed6c29495e54cc05947f18c8af](http://127.0.0.1:5984/shopcart/6e1295ed6c29495e54cc05947f18c8af)`
```

Here is the server’s response:

```sh
`{``"_id"``:``"6e1295ed6c29495e54cc05947f18c8af"``,``"_rev"``:``"1-4b39c2971c9ad54cb37e08fa02fec636"``,``"title"``:``"There is Nothing Left to Lose"``,``"artist"``:``"Foo Fighters"``}`
```

## 7\. Revisions

If we want to change a document in CouchDB, we don’t tell it to go and find a field in a specific document and insert a new value. Instead, we load the full document out of CouchDB, make our changes in the JSON structure (or object, when we are doing actual programming), and save the entire new revision (or version) of that document back into CouchDB. Each revision is identified by a new  __rev_ value.

If we want to update or delete a document, CouchDB expects us to include the__rev_ field of the revision we wish to change. When CouchDB accepts the change, it will generate a new revision number. This mechanism ensures that, in case somebody else made a change without us knowing before we got to request the document update, CouchDB will not accept our update because we are likely to overwrite data we didn’t know that even existed. Or simplified: whoever saves a change to a document first, wins. Let’s see what happens if we don’t provide a  __rev_ field (which is equivalent to providing a outdated value):

```sh
`curl -X PUT  [http://127.0.0.1:5984/shopcart/6e1295ed6c29495e54cc05947f18c8af](http://127.0.0.1:5984/shopcart/6e1295ed6c29495e54cc05947f18c8af)  -d` `'{"title":"There is Nothing Left to Lose","artist":"Foo Fighters","year":"1997"}'`
```

CouchDB replies:

```sh
`{``"error"``:``"conflict"``,``"reason"``:``"Document update conflict."``}`
```

If we see this, add the latest revision number of your document to the JSON structure:

```sh
`curl -X PUT  [http://127.0.0.1:5984/shopcart/6e1295ed6c29495e54cc05947f18c8af](http://127.0.0.1:5984/shopcart/6e1295ed6c29495e54cc05947f18c8af)  -d` `'{"_rev":"1-2902191555","title":"There is Nothing Left to Lose", "artist":"Foo Fighters","year":"1997"}'`
```

Now we see why it was handy that CouchDB returned that  __rev_  when we made the initial request. CouchDB replies:

```sh
`{``"ok"``:``true``,``"id"``:``"6e1295ed6c29495e54cc05947f18c8af"``,``"rev"``:``"2-2739352689"``}`
```

### 7.1 Accessing Previous Revisions

The above example gets the current revision. We may be able to get a specific revision by using the following syntax:

```sh
`GET /somedatabase/some_doc_id?rev=946B7D1C HTTP/1.0`
```

To find out what revisions are available for a document, we can do:

```sh
`GET /somedatabase/some_doc_id?revs_info=``true`  `HTTP/1.0`
```

This returns the current revision of the document, but with an additional  _\_revs\_info_  field, whose value is an array of objects, one per revision. For example:

`1`

`{`

`2`

`"_revs_info"``: [`

`3`

`{``"rev"``:` `"3-ffffff"``,` `"status"``:` `"available"``},`

`4`

`{``"rev"``:` `"2-eeeeee"``,` `"status"``:` `"missing"``},`

`5`

`{``"rev"``:` `"1-dddddd"``,` `"status"``:` `"deleted"``},`

`6`

`]`

`7`

`}`
```

Here,  _available_ means the revision content is stored in the database and can still be retrieved. The other values indicate that the content of that revision is not available.

Alternatively, the _revisions field, used by the replicator, can return an array of revision IDs more efficiently. The numeric prefixes are removed, with a “start” value indicating the prefix for the first (most recent) ID:

```sh
`{`

`2`

`"_revisions"``: {`

`3`

`"start"``: 3,`

`4`

`"ids"``: [``"fffff"``,` `"eeeee"``,` `"ddddd"``]`

`5`

`}`

`6`

`}`
```

We can fetch the bodies of multiple revisions at once using the parameter open\_revs=\[“rev1″,”rev2”,…\], or We can fetch all leaf revisions using open\_revs=all. The JSON returns an array of objects with an “ok” key pointing to the document, or a “missing” key pointing to the rev string.

```sh
`[`

`2`

`{``"missing"``:``"1-fbd8a6da4d669ae4b909fcdb42bb2bfd"``},`

`3`

`{``"ok"``:{``"_id"``:``"test"``,``"_rev"``:``"2-5bc3c6319edf62d4c624277fdd0ae191"``,``"hello"``:``"foo"``}}`

`4`

`]`
```

### 7.2 HEAD

A HEAD request returns basic information about the document, including its current revision.

```sh
`HEAD /somedatabase/some_doc_id HTTP/1.0`

`2`

`HTTP/1.1 200 OK`

`3`

`Etag:` `"946B7D1C"`

`4`

`Date: Thu, 17 Aug 2006 05:39:28 +0000GMT`

`5`

`Content-Type: application/json`

`6`

`Content-Length: 256`
```

### 7.3 PUT

To create new document we can either use a  _POST_ operation or a  _PUT_ operation. To create/update a named document using the PUT operation, the URL must point to the document’s location.

The following is an example HTTP  _PUT_. It will cause the CouchDB server to generate a new revision ID and save the document with it.

```sh
`PUT /somedatabase/some_doc_id HTTP/1.0`

`02`

`Content-Length: 245`

`03`

`Content-Type: application/json`

`04`

`05`

`{`

`06`

`"Subject"``:``"I like Plankton"``,`

`07`

`"Author"``:``"Rusty"``,`

`08`

`"PostedDate"``:``"2006-08-15T17:30:12-04:00"``,`

`09`

`"Tags"``:[``"plankton"``,` `"baseball"``,` `"decisions"``],`

`10`

`"Body"``:``"I decided today that I don't like baseball. I like plankton."`

`11`

`}`
```

Here is the server’s response.

```sh
`HTTP/1.1 201 Created`

`2`

`Etag:` `"946B7D1C"`

`3`

`Date: Thu, 17 Aug 2006 05:39:28 +0000GMT`

`4`

`Content-Type: application/json`

`5`

`Connection: close`

`6`

`7`

`{``"ok"``:` `true``,` `"id"``:` `"some_doc_id"``,` `"rev"``:` `"946B7D1C"``}`
```

To update an existing document, we also issue a  _PUT_request. In this case, the JSON body must contain a  __rev_ property, which lets CouchDB know which revision the edits are based on. If the revision of the document currently stored in the database doesn’t match, then a  _409_ conflict error is returned.

If the revision number does match what’s in the database, a new revision number is generated and returned to the client.

For example:

```sh
`PUT /somedatabase/some_doc_id HTTP/1.0`

`02`

`Content-Length: 245`

`03`

`Content-Type: application/json`

`04`

`05`

`{`

`06`

`"_id"``:``"some_doc_id"``,`

`07`

`"_rev"``:``"946B7D1C"``,`

`08`

`"Subject"``:``"I like Plankton"``,`

`09`

`"Author"``:``"Rusty"``,`

`10`

`"PostedDate"``:``"2006-08-15T17:30:12-04:00"``,`

`11`

`"Tags"``:[``"plankton"``,` `"baseball"``,` `"decisions"``],`

`12`

`"Body"``:``"I decided today that I don't like baseball. I like plankton."`

`13`

`}`
```

Here is the server’s response if what is stored in the database is a revision  _946B7D1C_  of document  _some\_doc\_id_.

```sh
`HTTP/1.1 201 Created`

`2`

`Etag:` `"2774761002"`

`3`

`Date: Thu, 17 Aug 2006 05:39:28 +0000GMT`

`4`

`Content-Type: application/json`

`5`

`Connection: close`

`6`

`7`

`{``"ok"``:``true``,` `"id"``:``"some_doc_id"``,` `"rev"``:``"2774761002"``}`
```

And here is the server’s response if there is an update conflict (what is currently stored in the database is not revision  _946B7D1C_ of document  _some\_doc\_id_).

```sh
`HTTP/1.1 409 Conflict`

`2`

`Date: Thu, 17 Aug 2006 05:39:28 +0000GMT`

`3`

`Content-Length: 33`

`4`

`Connection: close`

`5`

`6`

`{``"error"``:``"conflict"``,``"reason"``:``"Document update conflict."``}`
```

There is a query option  _batch=ok_which can be used to achieve higher throughput at the cost of lower guarantees. When a  _PUT_(or a document  _POST_as described below) is sent using this option, it is not immediately written to disk. Instead it is stored in memory on a per-user basis for a second or so (or the number of docs in memory reaches a certain point). After the threshold has passed, the docs are committed to disk. Instead of waiting for the doc to be written to disk before responding, CouchDB sends an HTTP  _202 Accepted_ response immediately.

_batch=ok_ is not suitable for crucial data, but it ideal for applications like logging which can accept the risk that a small proportion of updates could be lost due to a crash. Docs in the batch can also be flushed manually using the  _\_ensure\_full_commit_ API.

### 7.4 POST

The  _POST_operation can be used to create a new document with a server generated DocID. To do so, the URL must point to the database’s location. To create a named document, use the  _PUT_method instead.

It is recommended that we avoid  _POST_when possible, because proxies and other network intermediaries will occasionally resend  _POST_requests, which can result in duplicate document creation. If our client software is not capable of guaranteeing uniqueness of generated UUIDs, use a  _GET_to  _/_uuids?count=100_to retrieve a list of document IDs for future  _PUT_  requests. Please note that the  _/_uuids_-call does not check for existing document ids; collision-detection happens when We are trying to save a document.

The following is an example HTTP  _POST_. It will cause the CouchDB server to generate a new DocID and revision ID and save the document with it.

```sh
`POST /somedatabase/ HTTP/1.0`

`02`

`Content-Length: 245`

`03`

`Content-Type: application/json`

`04`

`05`

`{`

`06`

`"Subject"``:``"I like Plankton"``,`

`07`

`"Author"``:``"Rusty"``,`

`08`

`"PostedDate"``:``"2006-08-15T17:30:12-04:00"``,`

`09`

`"Tags"``:[``"plankton"``,` `"baseball"``,` `"decisions"``],`

`10`

`"Body"``:``"I decided today that I don't like baseball. I like plankton."`

`11`

`}`
```

Here is the server’s response:

```sh
`HTTP/1.1 201 Created`

`2`

`Date: Thu, 17 Aug 2006 05:39:28 +0000GMT`

`3`

`Content-Type: application/json`

`4`

`Connection: close`

`5`

`6`

`{``"ok"``:``true``,` `"id"``:``"123BAC"``,` `"rev"``:``"946B7D1C"``}`
```

As of 0.11 CouchDB supports handling of multipart/form-data encoded updates. This is used by Futon and not considered a public API. All such requests must contain a valid Referer header.

### 7.5 DELETE

To delete a document, perform a  _DELETE_  operation at the document’s location, passing the  _rev_  parameter with the document’s current revision. If successful, it will return the revision id for the deletion stub.

```sh
`DELETE /somedatabase/some_doc?rev=1582603387 HTTP/1.0`
```

As an alternative we can submit the  _rev_  parameter with the etag header field  _If-Match_.

```sh

`DELETE /somedatabase/some_doc HTTP/1.0`

`2`

`If-Match:` `"1582603387"`
```

And the response:

```sh

`HTTP/1.1 200 OK`

`2`

`Etag:` `"2839830636"`

`3`

`Date: Thu, 17 Aug 2006 05:39:28 +0000GMT`

`4`

`Content-Type: application/json`

`5`

`Connection: close`

`6`

`7`

`{``"ok"``:``true``,``"rev"``:``"2839830636"``}`
```

Note: Deleted documents remain in the database forever, even after compaction, to allow eventual consistency when replicating. If we delete using the DELETE method above, only the \_id, \_rev and a deleted flag are preserved. If we deleted a document by adding “_deleted”:true then all the fields of the document are preserved. This is to allow, for example, to record the time we deleted a document, or the reason we deleted it.

### 7.6 COPY

Note that this is a non-standard extension to HTTP.

We can copy documents by sending an HTTP COPY request. This allows us to duplicate the contents (and attachments) of a document to a new document under a different document id without first retrieving it from CouchDB. Use the  _Destination_  header to specify the document that We want to copy to (the target document).

It is not possible to copy documents between databases and it is not (yet) possible to perform bulk copy operations.

```sh
`COPY /somedatabase/some_doc HTTP/1.1`

`2`

`Destination: some_other_doc`
```

If we want to overwrite an existing document, we need to specify the target document’s revision with a  _rev_  parameter in the  _Destination_  header:

```sh

`COPY /somedatabase/some_doc HTTP/1.1`

`2`

`Destination: some_other_doc?rev=rev_id`
```

The response in both cases includes the target document’s revision:

```sh
`HTTP/1.1 201 Created`

`2`

`Server: CouchDB/0.9.0a730122-incubating (Erlang OTP/R12B)`

`3`

`Etag:` `"355068078"`

`4`

`Date: Mon, 05 Jan 2009 11:12:49 GMT`

`5`

`Content-Type: text/plain;charset=utf-8`

`6`

`Content-Length: 41`

`7`

`Cache-Control: must-revalidate`

`8`

`9`

`{``"ok"``:``true``,``"id"``:``"some_other_doc"``,``"rev"``:``"355068078"``}`
```

### 7.7 All Documents

#### 7.6.1 all_docs

To get a listing of all documents in a database, use the special  _\_all\_docs_  URI. This is a specialized View so the Querying Options of the  [HTTP\_view\_API](http://wiki.apache.org/couchdb/HTTP_view_API)  apply here.

```sh
`GET /somedatabase/_all_docs HTTP/1.0`
```

This will return a listing of all documents and their revision IDs, ordered by DocID (case sensitive):

```sh
`HTTP/1.1 200 OK`

`02`

`Date: Thu, 17 Aug 2006 05:39:28 +0000GMT`

`03`

`Content-Type: application/json`

`04`

`Connection: close`

`05`

`06`

`{`

`07`

`"total_rows"``: 3,` `"offset"``: 0,` `"rows"``: [`

`08`

`{``"id"``:` `"doc1"``,` `"key"``:` `"doc1"``,` `"value"``: {``"rev"``:` `"4324BB"``}},`

`09`

`{``"id"``:` `"doc2"``,` `"key"``:` `"doc2"``,` `"value"``: {``"rev"``:``"2441HF"``}},`

`10`

`{``"id"``:` `"doc3"``,` `"key"``:` `"doc3"``,` `"value"``: {``"rev"``:``"74EC24"``}}`

`11`

`]`

`12`

`}`
```

Use the query argument  _descending=true_  to reverse the order of the output table:

Will return the same as before but in reverse order:

```sh
`HTTP/1.1 200 OK`

`02`

`Date: Thu, 17 Aug 2006 05:39:28 +0000GMT`

`03`

`Content-Type: application/json`

`04`

`Connection: close`

`05`

`06`

`{`

`07`

`"total_rows"``: 3,` `"offset"``: 0,` `"rows"``: [`

`08`

`{``"id"``:` `"doc3"``,` `"key"``:` `"doc3"``,` `"value"``: {``"rev"``:``"74EC24"``}},`

`09`

`{``"id"``:` `"doc2"``,` `"key"``:` `"doc2"``,` `"value"``: {``"rev"``:``"2441HF"``}},`

`10`

`{``"id"``:` `"doc1"``,` `"key"``:` `"doc1"``,` `"value"``: {``"rev"``:` `"4324BB"``}}`

`11`

`]`

`12`

`}`
```

The query string parameters  _startkey_,  _endkey_  and  _limit_  may also be used to limit the result set. For example:

```sh
`GET /somedatabase/_all_docs?startkey=``"doc2"``&amp;limit=2 HTTP/1.0`

Will return:

`01`

`HTTP/1.1 200 OK`

`02`

`Date: Thu, 17 Aug 2006 05:39:28 +0000GMT`

`03`

`Content-Type: application/json`

`04`

`Connection: close`

`05`

`06`

`{`

`07`

`"total_rows"``: 3,` `"offset"``: 1,` `"rows"``: [`

`08`

`{``"id"``:` `"doc2"``,` `"key"``:` `"doc2"``,` `"value"``: {``"rev"``:``"2441HF"``}},`

`09`

`{``"id"``:` `"doc3"``,` `"key"``:` `"doc3"``,` `"value"``: {``"rev"``:``"74EC24"``}}`

`10`

`]`

`11`

`}`
```

Use  _endkey_  if you are interested in a specific range of documents:

```sh
`GET /somedatabase/_all_docs?startkey=``"doc2"``&amp;endkey=``"doc3"`  `HTTP/1.0`

This will get keys inbetween and including doc2 and doc3; e.g.  _doc2-b_  and  _doc234_.
```

Both approaches can be combined with  _descending_:

```sh
`GET /somedatabase/_all_docs?startkey=``"doc2"``&amp;limit=2&amp;descending=``true`  `HTTP/1.0`
```

Will return:

```sh

`HTTP/1.1 200 OK`

`02`

`Date: Thu, 17 Aug 2006 05:39:28 +0000GMT`

`03`

`Content-Type: application/json`

`04`

`Connection: close`

`05`

`06`

`{`

`07`

`"total_rows"``: 3,` `"offset"``: 1,` `"rows"``: [`

`08`

`{``"id"``:` `"doc3"``,` `"key"``:` `"doc3"``,` `"value"``: {``"rev"``:``"74EC24"``}},`

`09`

`{``"id"``:` `"doc2"``,` `"key"``:` `"doc2"``,` `"value"``: {``"rev"``:``"2441HF"``}}`

`10`

`]`

`11`

`}`
```

If we add  _include_docs=true_  to a request to  _\_all\_docs_  not only metadata but also the documents themselves are returned.

#### 7.6.2 _changes

This allows us to see all the documents that were updated and deleted, in the order these actions are done:

```sh
`GET /somedatabase/_changes HTTP/1.0`
```

Will return something of the form:

`01`

`HTTP/1.1 200 OK`

`02`

`Date: Fri, 8 May 2009 11:07:02 +0000GMT`

`03`

`Content-Type: application/json`

`04`

`Connection: close`

`05`

`06`

`{``"results"``:[`

`07`

`{``"seq"``:1,``"id"``:``"fresh"``,``"changes"``:[{``"rev"``:``"1-967a00dff5e02add41819138abb3284d"``}]},`

`08`

`{``"seq"``:3,``"id"``:``"updated"``,``"changes"``:[{``"rev"``:``"2-7051cbe5c8faecd085a3fa619e6e6337"``}]},`

`09`

`{``"seq"``:5,``"id"``:``"deleted"``,``"changes"``:[{``"rev"``:``"2-eec205a9d413992850a6e32678485900"``}],``"deleted"``:``true``}`

`10`

`],`

`11`

`"last_seq"``:5}`
```


All the view parameters work on \_changes, such as startkey, include\_docs etc. However, note that the startkey is exclusive when applied to this view. This allows for a usage pattern where the startkey is set to the sequence id of the last doc returned by the previous query. As the startkey is exclusive, the same document won’t be processed twice.

## 8\. Replication

CouchDB replication is a mechanism to synchronize databases. Much like  _rsync_synchronizes two directories locally or over a network, replication synchronizes two databases locally or remotely.

In a simple POST request, we tell CouchDB the  _source_  and the  _target_  of a replication and CouchDB will figure out which documents and new document revisions are on  _source_  that are not yet on  _target_, and will proceed to move the missing documents and revisions over.

First, we will create a target database. Note that CouchDB would not automatically create a target database for us, and will return a replication failure if the target doesn’t exist:

```sh
`curl -X PUT  [http://127.0.0.1:5984/shopcart-replica](http://127.0.0.1:5984/shopcart-replica)`
```

Now we can use the database  _albums-replica_  as a replication target:

```sh
`curl -vX POST  [http://127.0.0.1:5984/_replicate](http://127.0.0.1:5984/_replicate)  -d` `'{"source":"shopcart","target":"shopcart-replica"}'`  `-H` `"Content-Type: application/json"`
```









## POC de validation avec le couple Gitbook/Nuts
#Hebergement du Blog sur un Gitbook/nuts

[![Create CouchDB Database](https://www.webcodegeeks.com/wp-content/uploads/2015/09/image004.png.webp)](http://www.webcodegeeks.com/wp-content/uploads/2015/09/image004.png)

Créer une base de données CouchDB

Tapez le nom de la base de données (c'est-à-dire `blog` ici) dans la zone de texte que nous voulons créer.


Une fois que nous avons créé la base de données (ou sélectionné une base existante), nous recevrons la liste des documents en cours. Si nous créons un nouveau document ou si nous sélectionnons un document existant, l'affichage du document d'édition nous sera présenté.

Pour traiter les documents dans Futon, vous devez sélectionner le document, puis modifier (et paramétrer) les zones du document individuellement avant de le sauvegarder à nouveau dans la base de données.

Par exemple, la figure ci-dessous montre l'éditeur pour un seul document, un document nouvellement créé avec un seul ID, le champ Document _id (cliquez sur l'image pour l'afficher en taille réelle).

[![New document](https://www.webcodegeeks.com/wp-content/uploads/2015/09/image006-300x55.png.webp)](http://www.webcodegeeks.com/wp-content/uploads/2015/09/image006.png)

__Nouveau document__
Pour ajouter un champ au document:

1.  Cliquez sur Ajouter un champ.
2.  Dans la zone Nom du champ, saisissez le nom du champ. Par exemple, "nom_blogueur".
3.  Cliquez sur la coche verte à côté du nom du champ pour confirmer le changement de nom du champ.

[![Changing a field name](https://www.webcodegeeks.com/wp-content/uploads/2015/09/image008.png.webp)](http://www.webcodegeeks.com/wp-content/uploads/2015/09/image008.png)

4.  Double-cliquez sur la cellule de valeur correspondante.
5.  Saisissez un nom de société, par exemple "La-Fabrik_Digital".


[![Field name change](https://www.webcodegeeks.com/wp-content/uploads/2015/09/image011.jpg.webp)](http://www.webcodegeeks.com/wp-content/uploads/2015/09/image011.jpg)


6.  Cliquez sur la coche verte à côté de la valeur du champ pour confirmer la valeur du champ.
7.  Nous devons enregistrer explicitement le document en cliquant sur le bouton Enregistrer le document en haut de la page. Ceci sauvegardera le document, puis affichera le nouveau document avec les informations de révision sauvegardées (champ _rev). (Cliquez sur l'image pour l'agrandir)

[![Document changes](https://www.webcodegeeks.com/wp-content/uploads/2015/09/image012-300x78.png)](http://www.webcodegeeks.com/wp-content/uploads/2015/09/image012.png)









## POC de validation avec le couple Gitbook/Nuts
#Hebergement du Blog sur un Gitbook/nuts

## Procedure d'essai sur le compte philuser

Mise en place d'un Gitbook [nuts](https://nuts.gitbook.com)

Creation d'un repository philuser/nuts : https://github.com/philuser/nuts

Pour un deployment de Nuts avec Heroku, voir la Doc : https://nuts.gitbook.com/deploy.html

Creation du personal Tokens 'philonuts — repo' sous : https://github.com/settings/tokens

Pour ce Token le GITHUB_TOKEN est :

Access token for GitHub API : ecd239b2cdee7465a87e20d25c2b400254af4e33

### Repredre le process avec un nouveau compte "Fabrik_Digital"


<!---
 - [](https://www.netlify.com/blog/2016/05/18/9-reasons-your-site-should-be-static/)
https://www.webcodegeeks.com/web-development/couchdb-installation-how-to-install-couchdb/
--->

## Alternatives - Solutions Tiers

### Netlify
 - [Netlify - Build, deploy, and manage modern web projects](https://www.netlify.com/)
 - [Netlify - Static without limits](https://www.netlify.com/features/)
 -

### [**Ddoc Lab** 1.8β](http://ddoc.me/)

### Qu'est-ce que Ddoc Lab ?

Ddoc Lab est un outil manquant dans l'univers CouchDB. Il s'agit d'un IDE spécialisé pour la création, la validation, la construction et la publication de couchapps et de documents de conception. Avec Ddoc Lab vous n'avez besoin que d'un navigateur pour composer un couchapp.

Ddoc Lab prend en charge la minification du code, la récupération à distance des sources et des capacités de post-traitement complètes.

Ddoc Lab est noBackend offline-first app. Cela signifie qu'une fois que vous l'ouvrez, vous pouvez éditer et persister vos projets indépendamment de votre connexion web. Merci PouchDB pour cela.

### Concept principal

Ddoc Lab détient vos projets en tant que documents JSON avec pièces jointes. Ils sont tous stockés dans la base de données interne de votre navigateur local, qui peut être synchronisé avec Couch à distance.

Chaque projet Ddoc Lab est un ensemble de textes ou d'éléments binaires. Les éléments de texte sont dactylographiés - ils peuvent être JS, JSON, HTML, CSS, markdown, moins ou simplement du texte brut. "Dactylographié" signifie la mise en évidence de la syntaxe, la validation, la compression, la conversion appropriée en JSON et ainsi de suite.

Pour chaque élément, vous attribuez le chemin - une place pour un élément dans une construction finale. Donc pour faire une vue, vous n'avez qu'à assigner un chemin comme .views.myView.map à votre élément. Pour mettre votre code en pièce jointe, vous n'avez qu'à assigner un chemin comme /js/myscript.js.

### Comment ça se construit

Lorsque vous cliquez sur le bouton ![fi-upload-cloud](https://dc585.4shared.com/img/6_KLddI0ei/s24/16296172510/fi-upload-cloud-16?async&rand=0.5379159785660923) `

Couch` , votre projet est résolu en un seul JSON et fusionné avec CouchDB doc distant. Le terme'fusionné' signifie que s'il existe déjà une documentation à distance, votre projet local ne remplace que les branches, définies dans le projet. Les autres branches d'un document distant sont conservées intactes.

### Inclure des directives
Chaque élément peut inclure d'autres éléments. Pour inclure d'autres éléments textuellement, il suffit de taper `{{/chemin/toitem.js}}` - et cette directive est remplacée par le texte intégral de `toitem.js` sur build.

![](https://dc585.4shared.com/img/TDIxswEOca/s24/16296295550/ddoc-Lab18?async&rand=0.46527604166717484)
>En arrière-plan - code source avec les directives include. Dans la fenêtre contextuelle de prévisualisation, tous les includes sont résolus.

Vous pouvez sûrement configurer n'importe quel élément pour qu'il soit non publiable - pour garantir que votre fragment de code n'est construit qu'en tant qu'include, et non en tant que pièce jointe autonome ou branche ddoc.

### Récupération à distance
L'élément peut être un pointeur vers une ressource externe - comme une requête http. Si vous allez chercher JSON, vous pouvez aussi définir un chemin à l'intérieur de la ressource à recupérer. Les documents récupérés sont mis en cache à l'intérieur d'un projet.

### Post-traitement
Chaque élément de texte peut avoir son propre post-processeur javascript. C'est une fonction définie par l'utilisateur, qui prend la représentation de la chaîne de caractères de l'élément, la manipule et renvoie une autre chaîne de caractères.

Fonction utile pour transformer les données à distance ou vérifier les données locales.

### Autres caractéristiques
#### Pré-construction.
Ddoc Lab peut pré-construire à la fois l'ensemble du document ou n'importe quel élément - et vous montrer le résultat. Ou une liste de toutes les erreurs, le cas échéant. Vous pouvez également afficher toutes les dépendances et les personnes à charge d'un poste.

#### Importer des ddocs.
Vous pouvez importer n'importe quel document ou ddoc CouchDB existant, et il sera divisé en items+chemins lors de l'importation. Les pièces jointes non binaires deviennent modifiables.

#### Télécharger le résultat.
Vous pouvez télécharger votre couchapp sous forme de fichier `.json` ou d'archive `.tar` en un seul clic.

###  Comment utiliser Ddoc Lab
La version complète de [Ddoc Lab est disponible ici](http://ddoc.me/lab/). Nous ne stockons aucun de vos documents, ils persistent tous dans votre navigateur - donc aucun enregistrement n'est nécessaire. Pour mieux comprendre ce qu'est quoi, ouvrez doc Demo project à partir du volet droit de l'éditeur.

Vous pouvez également [télécharger Ddoc Lab en tant que JSON unique](http://ddoc.me/json.html), que vous pouvez mettre dans votre CouchDB et avoir Ddoc Lab en place.

### Tableau de comparaison des differentes solutions

|__Comparaison avec d'autres solutions__ |[Ddoc Lab](http://ddoc.me/)| [Python Couchapp](https://github.com/couchapp/couchapp)| [CouchDB Bootstrap](https://github.com/eHealthAfrica/couchdb-bootstrap)| [Erica](https://github.com/benoitc/erica)
|:---|:---:|:---:|:---:|:---:|
|Technologie support __Backend__|CouchDB|Python|Node.js|Erlang OTP|
|Environnement de développement -> navigateur interactif...| ✔︎ |  |  | ✔︎ |
|...avec mode hors ligne| ✔︎ |  |  |  |
|Prise en charge de plusieurs bases de données cibles par session.| ✔︎ |  | ✔︎ |  |
|Peut fusionner avec ddoc existant.| ✔︎ |  |  |  |
|Construction incrémentale| ✔︎ |  |  |  |
|Gestion des fichiers d'inclusions| ✔︎ |  |  |  |
|Inclusion des ressources externes| ✔︎ |  |  |  |
|Code/Translateur de format de données/post-processeurs| ✔︎ |  |  |  |
|Possibilité d'importer des ddocs existants| ✔︎ |  |  |
Configuration de la base de données à distance et gestion des utilisateurs|  |  | ✔︎ |  |
|Bien documenté|  |  | ✔︎ |  |
|Peut se construire à partir d'un repo git|  |  |  | ✔︎ |
|Possède un mode d'auto-construction| ✔︎ |  |  |  |
-->> Written with [StackEdit](https://stackedit.io/).
