+++
title = "Site statique"
weight = 12
chapter = true
+++

 Depuis quelques mois, je cherche un moyen de concilier stockage électronique cohérent (sur disque  dur) et édition via un site web.
 Je ne souhaitais pas un systeme avec base de données, trop compliqué.
 J'ai donc découvert les **sites dits statiques** :

  * <https://jamstatic.fr/>
  * <https://www.staticgen.com/>

Pour éviter la location d'un serveur d'hebergement, je me suis tourné vers **gitlab**.

Je me suis interessé à Jekyll, puis Gitbook, pour finalement adopter **Hugo** : celui-ci permet simplement de reproduire son arborescence personnelle avce celle son site Web, très pratique et simple.
