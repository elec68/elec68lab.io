/*
 * PEM-encoded client certificate.
 *
 * Must include the PEM header and footer:
 * "-----BEGIN CERTIFICATE-----"
 * "...base64 data..."
 * "-----END CERTIFICATE-----";
 */
static const char clientcredentialCLIENT_CERTIFICATE_PEM[] = 
"-----BEGIN CERTIFICATE-----\n"
"MIIDWjCCAkKgAwIBAgIVAKfUCJrpBOKjvcf/QoJ7GDbgWIOPMA0GCSqGSIb3DQEB\n"
"CwUAME0xSzBJBgNVBAsMQkFtYXpvbiBXZWIgU2VydmljZXMgTz1BbWF6b24uY29t\n"
"IEluYy4gTD1TZWF0dGxlIFNUPVdhc2hpbmd0b24gQz1VUzAeFw0xODA2MjcxNzE2\n"
"MTZaFw00OTEyMzEyMzU5NTlaMB4xHDAaBgNVBAMME0FXUyBJb1QgQ2VydGlmaWNh\n"
"dGUwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQC4mditxUDPljDl1d+n\n"
"ks2YmLj6sBZs/31J/hS3oZGlUiGWXD7WSG0Zt+zcoUS6gWOr0NYnNyTtaQ5afq0q\n"
"wYCvGt2ZxcNnjGu2O/dttTye94bewVFUvlP4bTJGECDCrUV2M4Wx5VspeOWA1Uer\n"
"SFgCDVCSL0GF3Kc7ylizdpkRZFdbNcpssPNRn6c4AwSDNBJwNzYbhQ/CMJTf2DDx\n"
"Q4moMGTzUftH+4SXIou+fEdR8+HoehUc3bH7sbr89mePuR0yBaTQDMBV99Ylygrp\n"
"x3RNYyeqPWFOZB2FQrY3UjL5nSMGVLUzZQCJGUxklGZ3BPpl+lH9YxJTnX7GYQLu\n"
"0REjAgMBAAGjYDBeMB8GA1UdIwQYMBaAFNz8yK1Fzc2dntVa4PeasmCrB5p8MB0G\n"
"A1UdDgQWBBQNzIi3V7IyDKR37mg0QEtYMgv9IDAMBgNVHRMBAf8EAjAAMA4GA1Ud\n"
"DwEB/wQEAwIHgDANBgkqhkiG9w0BAQsFAAOCAQEAYQJ/58mptKGSLXUzY4H3Jtt6\n"
"iaq9yONAGhpFjY9FfCrczrxM1+EctGAponsHKK8YG+T3Mt602wyQowvGxh9ZkaLw\n"
"juvptH7wPuYev/2dViD+slkqb7YUs5tjnHvYVAgKvSjtIdOJaWsYkqJwzBqb0Fom\n"
"rqHFAlrxLpYLDmmr8/wPT/CVWWWiAy0sQ8SaEsSoUiNpGuxjrGUJ/7dAjAVp9Yw3\n"
"GHsAHDoe10ASlnqoCEUZBjc5TYNzIRV1tOfXjCTU3SZv2uinvvZiWWjBuZuCPLU7\n"
"4GdnI5PyQlDCxLvNHgj2v9SBC6KapOopTtychbmauA+wejxwlzKhZpd4DrySbw==\n"
"-----END CERTIFICATE-----";

/*
 * PEM-encoded client private key.
 *
 * Must include the PEM header and footer:
 * "-----BEGIN RSA PRIVATE KEY-----"
 * "...base64 data..."
 * "-----END RSA PRIVATE KEY-----";
 */
static const char clientcredentialCLIENT_PRIVATE_KEY_PEM[] =
"-----BEGIN RSA PRIVATE KEY-----\n"
"MIIEowIBAAKCAQEAuJnYrcVAz5Yw5dXfp5LNmJi4+rAWbP99Sf4Ut6GRpVIhllw+\n"
"1khtGbfs3KFEuoFjq9DWJzck7WkOWn6tKsGArxrdmcXDZ4xrtjv3bbU8nveG3sFR\n"
"VL5T+G0yRhAgwq1FdjOFseVbKXjlgNVHq0hYAg1Qki9BhdynO8pYs3aZEWRXWzXK\n"
"bLDzUZ+nOAMEgzQScDc2G4UPwjCU39gw8UOJqDBk81H7R/uElyKLvnxHUfPh6HoV\n"
"HN2x+7G6/PZnj7kdMgWk0AzAVffWJcoK6cd0TWMnqj1hTmQdhUK2N1Iy+Z0jBlS1\n"
"M2UAiRlMZJRmdwT6ZfpR/WMSU51+xmEC7tERIwIDAQABAoIBACsDtkiviO2WGqjD\n"
"kEz02TFwjNHlTCRz2BbwvJUZcd2q6KansgCuebWfdPLoJSKsWbREglI6+EDV1QFj\n"
"OSiYVEr5IJ/fRCwRvVuGV55Zt9l25hdrxm7PtrNRhoBq66dRRrlHQiT0w6xDPn8p\n"
"s3yG6/L2dUrK9iKiCtnUyZxvyzUQ3hzmn+qP/mt1Dt6lRet9Sm4wD95iChxkXbtK\n"
"Vrii1jU/ZLgKpHXWslBKBU7MwsCuCawdenOovCP+MgMmaXf6Q+nyGFDniuvKG5vH\n"
"uSm7hxDL+r5lU0cZk8dWBztdplNlBdi3W/lGEtZpIdV11sOVq5G0Gun+1alW0HSn\n"
"nvwuO5kCgYEA40stIcRPSvyRESkCjZB+cexOCsUd6INezS+c1i5hl+3NYHIooQAf\n"
"+xmdMXH2m7nKZuEuoFMf/dUaIDtA/wfdYge3Ghk0X4C54UXH4bXZ3wy6lqKsWRd0\n"
"8JyzGlYfgXlx6rkN6n54dIHoARH1mDVmw7k/1jbNZvmQaFDREJBy+s0CgYEAz+pW\n"
"IJ3Jy2lMtROcBfKBOYb4aBlM6OB2/DcUJNDWZYfvNr0E3cBJPKDnWAnvaDaZqHSx\n"
"WpV8MbMM6uHA/53F6A4gb+AacoNiRutAWwizDBXxjAte6e6Z1Tw3L+W+NPYuHbO7\n"
"whARaKGwMS2T7Ba6m98b8BkZrFrMKjuO6dbUG68CgYBOVs7LYJnjwoFNZNEYccDj\n"
"Cwsq8x2H0h+OoOZxe7LvZmru3unZ8+0R4pYnQJw6uCJmqx4i7AOOvTAwA7w3KkLH\n"
"UtltCVrfv8JRPnfR95eMJRr6aD72xfrsI1YnsZ9SRMVanUWKmORzY4LPO51mRKGS\n"
"u/qgnmrVHxcCnTgxeVfn0QKBgQDPwbqEZwcwwizLOCgM7VDhehQYeUeX4EwlJKh1\n"
"TVUfKWEIkoIdT7oe4i2plGvs97o9UTWzbXIne2IqBqHDCxWqFwuknwvuZ4i5kvPA\n"
"w4WeSwMNzILwQOLo533B19FjvKdCOMoiVcn/gqm7ym5oWCHqBVw2Y+56glRG0zKw\n"
"SWGzswKBgDbkY7AqMdrTxehjqu67dRKzQbOMHBo3ac0k7HVrbJlTclOore8ugHuV\n"
"npvXw1R3bI21nWlbjLjhKCtj+4pNNCDcmzoM1FM0GGFhcExFsy9FSDagpCdVFpUw\n"
"Nmh//C5Qdq3WD5yCet06NHMCf+DVQ4G+jW5OJ5pHU8jzgHnovFuX\n"
"-----END RSA PRIVATE KEY-----";

/*
 * PEM-encoded Just-in-Time Registration (JITR) certificate (optional).
 *
 * If used, must include the PEM header and footer:
 * "-----BEGIN CERTIFICATE-----"
 * "...base64 data..."
 * "-----END CERTIFICATE-----";
 */
static const char * clientcredentialJITR_DEVICE_CERTIFICATE_AUTHORITY_PEM = NULL;