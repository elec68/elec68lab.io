+++
title = "installationDansArduino"
weight = 15
+++

 

Start Arduino and open Preferences window.

Enter http://arduino.esp8266.com/stable/package_esp8266com_index.json into Additional Board Manager URLs field. You can add multiple URLs, separating them with commas.

Open Boards Manager from Tools > Board menu and install esp8266 platform (and don't forget to select your ESP8266 board from Tools > Board menu after installation).


__Documentation: https://arduino-esp8266.readthedocs.io/en/2.5.0/__