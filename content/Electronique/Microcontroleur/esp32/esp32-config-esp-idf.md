+++
title = "esp32-config-esp-idf"
weight = 15

+++

installation necessaire :

    sudo pacman -S --needed gcc git make ncurses flex bison gperf python2-pyserial

Variables à créer au préalable:

    export IDF_PATH=/run/media/rp/Part2To/Electronique/MicroControleur/esp/esp32/esp-idf

    export PATH=$PATH:/run/media/rp/Part2To/Electronique/MicroControleur/esp/esp32/xtensa-esp32-elf/bin

(dans /etc/profile, si oushait de permanence)

Dl :

https://github.com/espressif/esp-idf.git  
https://dl.espressif.com/dl/xtensa-esp32-elf-linux64-1.22.0-73-ge28a011-5.2.0.tar.gz


PUIS , dans l'exemple choisi, faire la config via :

    make menuconfig

Template:  

    git clone --recursive https://github.com/espressif/esp-idf-template.git elektor1