+++
title = "esp32-arduino-eclipse"
weight = 15

+++

http://www.irelandupclose.com/customer/esp32/cpp-arduino/Eclipse-Arduino.html

BUILDING AN ARDUINO APPLICATION IN ECLIPSE



A. MAKE C++ TEMPLATE APPLICATION


    Install latest toolchain and esp-idf into ~/esp

    Make blink project from esp-idf:
    cd ~/esp &&
    rm -r -f blink &&
    cp -r esp-idf/examples/get-started/blink blink &&
    cd blink

    Create a components folder in the project folder and clone this repository inside it:
    mkdir -p components &&
    cd components &&
    git clone https://github.com/espressif/arduino-esp32.git arduino &&
    cd arduino &&
    git submodule update --init --recursive &&
    cd ../..

    make menuconfig

    Go into Arduino Configuration:

        Autostart Arduino setup and loop on boot: off

        Disable mutex locks for HAL: off

        Include only specific Arduino libraries: on

            Enable SD_MMC: off (if this is causing a problem in Make)

    SDK tool configuration: make sure Compiler toolchain path/prefix is 'xtensa-esp32-elf-'

    Rename blink.c to blink.cpp

    Edit blink.cpp to be like

#include "Arduino.h"
extern "C" void app_main()
{
initArduino();
pinMode(4, OUTPUT);
digitalWrite(4, HIGH);
//do your own thing
}


    Make & flash
    If make fails, clear the build folders and try again:
    rm -rf build




B. EDIT CODE TO MAKE IT INTO AN ARDUINO/C++ APPLICATION


1. Copy the blink application to your Eclipse workspace


2. In Eclipse do:

    File/Import/CC++/Existing Code as a Makefile Project

    Next

    Browse to the blink folder in your workspace (the Project Name will automatically be put in).

    Under Toolchain for Indexer Settings select Cross GCC

    Click Finish

3. Rename blink.cpp file to your own application name and replace the contents with your own Arduino/C++ sketch.


4. Add "#include "Arduino.h" at the top and make sure you have extern ‘C’ void app_main and initArduino() as in A. Above. Also make calls to setup() and loop() if you have these in your sketch.



C. SET UP BUILD ENVIRONMENT TO BUILD A C++ APP


1. Right-click the project in the Project Explorer tab

    Choose Properties

    Choose C/C++ Build

    Choose Environment

    Choose Add

        Name: v

        Value: 1

        Select Add to all configurations

        OK

    Choose Add again

        Name: IDF_PATH

        Value: enter the full path where you have installed the EPS-IDF (see 1. above). For instance: C:/msys32/home/Robert/esp/esp-idf (make sure to use '/', not '\') in Windows or ~/esp/esp-idf in Linux (note: the path may already be set)

        Select Add to all configurations

        OK

    Select the PATH variable and click on Edit

        Replace the Value with C:\msys32\usr\bin;C:\msys32\mingw32\bin;C:\msys32\opt\xtensa-esp32-elf\bin in Windows or /bin:/home/robert/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/home/robert/esp/xtensa-esp32-elf/bin (or the corresponding path if you have installed msys32 somewhere else ... obviously with your own username inserted instead of ‘robert’).

        Apply

    Select C/C++ General

        Preprocessor Include Paths

        Providers tab

        Click on CDT Cross GCC Built-in Compiler Settings

        Replace the Command to get compiler specs with

            xtensa-esp32-elf-gcc ${FLAGS} -E -P -v -dD "${INPUTS}

        Apply

        Click on CDT GCC Build Output Parser

        Replace the Compiler command pattern with

            xtensa-esp32-elf-(g?cc)|([gc]\+\+)|(clang)

            Apply

        Apply and Close



D. BUILD TARGET


1. Right-click on project

    Index/Rebuild

    Clean Project

2. Build Targets/Create

    Target name: all (this will be used to build the app)

3. Build Targets/Create

    Target name: flash (this will be used to build and flash the app)

4. Connect to EPS32


5. Check COM/tty port (for example using Device Manager/Ports in Windows, or ‘dmesg | grep tty’ in Linux)


6. Double-check that COM/tty port has been correctly set:

    Open sdkconfig and check/correct the entry in:

    CONFIG_ESPTOOLPY_PORT="COMx" / “/dev/ttyUSBx”


7. Click on the project

    Build Targets

    Flash (double-click)

8. Open a Terminal (you may need to go to Window/Show View/Terminal)

    Click on Terminal icon

    Choose terminal: Serial Terminal

    Set up values

    OK

If your program terminated at flash, restart the ESP32 to see the output (if any).


NOTE: if you program shows errors even though it built without any errors, rebuild the index (right-click on project , Index, Rebuild). If that doesn't fix the problem try cleaning and rebuilding the project. If that doesn't work, go to Project/ Properties/ C/C++ General/ Code Analysis and unclick/apply and click back on/apply Syntax and Semantic Errors: you can also turn off/apply individual entries until you find the one that is causing a problem. In some cases it's simply a question of overriding the 'error' in the project editor).



E. REPLICATE


In order to avoid having to do this every single time, copy the blink folder and call it ‘Arduino-template’ for instance (and save a copy securely!).


Then when you have a new project, say ‘myProject’, copy the ‘Arduino-template’ again and rename it to ‘myProject’.


Import it into Eclipse and set the Eclipse project properties as in C. Above.


Edit Makefile and change the PROJECT NAME: = myProject


Rebuild the index, clean the project and build it.



F. ADD 3rd-PARTY LIBRARY


Create, download or install the library in Arduino and copy it to the

myProject/components/components/libraries folder. Run make menuconfig and copy the project to your Eclipse workspace as in E. Above.




