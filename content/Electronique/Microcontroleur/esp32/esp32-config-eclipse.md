+++
title = "esp32-config-eclipse"
weight = 15

+++

Les configs à modifier :

https://dl.espressif.com/doc/esp-idf/latest/get-started/eclipse-setup.html?highlight=eclipse

BATCH_BUILD : value 1.


xtensa-esp32-elf-gcc ${FLAGS} -E -P -v -dD "${INPUTS}"

xtensa-esp32-elf-(g?cc)|([gc]\+\+)|(clang)
